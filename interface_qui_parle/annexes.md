% Annexes — Quand l'interface nous parle
% Vincent Maillard
% 2019


[→ Site web pour consulter les vidéos](https://vincent-maillard.fr/projets/interface_qui_parle/)


1. [Recueil - Patrick-Henri Burgaud, 2005](oldweb.today/nswin/20011018160024/http://www.aquoisarime.net/java/Sommaire.htm)

Une des entrées qui m'intéresse dans le projet de Burgaud est qu'il "s'adresse" à quelqu'un. Chacun des poèmes dit "je t'aime". On ne sait pas exactement qui parle ni si cela nous est vraiment destiné. Le poème "Secret" demande un mot de passe, lorsque l'on écrit "je t'aime", on est redirigé vers une page (secret.html) qui nous *répond* "moi aussi".


2. [Oui/non, conversation captive - Luc Dall'Armellina, 2003](http://lucdall.free.fr/oui/index.htm)

Ce site web s'adresse explicitement à l'internaute, lui demande son prénom, et l'emmène dans une lecture séquencée au clic, sur le ton de la confidence. Le site web fait appel à la fonction JavaScript *alert()*. Cet élément est familier et change en fonction des navigateurs, si bien que le graphisme de cette œuvre n'est pas fixé.


3. [My boyfriend came back from the war - Olia Lialina, 1996](http://www.teleportacia.org/war/)

Quelqu'un s'adresse à son petit ami, mais l'interaction au clic et le "you" parfois ambiguë apporte un flou : est-ce que ça pourrait s'adresser au lecteur ? En tout cas, il y a un rapport au message. J'ai l'impression que ce site web n'est pas seulement un récit (comme pourrait le laisser penser l'introduction), il cherche surtout à créer / simuler une conversation et produit une forme de silence en retour. Le collectif Foundland a repris le thème avec [Baby come home](https://vimeo.com/159500094), 2016. Pour moi ce n'est pas un hasard qu'il mette en forme la conversation sous forme de conversation style Whats'App. J'ai l'impression qu'il y a une équivalence entre les interactions de My Boyfriend et une conversation par message.


4. [The Machine is Us/ing Us - Michael Wesch, 2007](https://www.youtube.com/watch?v=NLlGopyXT_g)

Cette vidéo s'apparente aux "[desktop films](http://www.imdb.com/list/ls034008089/)" c'est-à-dire qu'elle est "filmée" en utilisant l'affichage de l'écran lui-même. Le contenu de celle-ci est une mise en avant des possibilités du numérique et notamment de l'écriture numérique. Elle concrétise cette écriture : la démonstration est la preuve elle-même de son propos.\
Je la trouve également intéressante pour le point de vue qu'elle adopte. Une sorte de vue subjective numérique. Un narrateur nous raconte quelque chose : on suit sa souris, ce qu'il écrit au clavier. C'est ainsi qu'il attire notre attention. Les codes numériques comme le clic, le formulaire, le zoom deviennent des moyens d'expression qui simulent une forme d'oralité.


5. [Noah - Patrick Cederberg, 2013](https://vimeo.com/65935223)

Ce court métrage est un autre *desktop film*. Il raconte la rupture toute numérique de Noah et de sa copine et ce selon le point de vue de Noah, c'est comme si l'on suivait son regard sur son écran. Au-delà de la surabondance d'interfaces et de contenus, j'en retiens le trouble de la fiction : est-ce que ça se passe vraiment ? Le récit nous immerge dans des sons, des couleurs, des typographies qui nous sont familiers. On est aussi familier de l'aspect purement fonctionnel et performatif de ces interfaces : on écrit ici, et ça écrira là-bas. Si bien qu'il y a comme une confiance crédule envers les interfaces qui valide ce qui se passe à l'écran.\
En reprenant du vocabulaire de théâtre, j'ai l'impression que ces interfaces construisent une double énonciation : le personnage écrit à un autre mais les codes de l'interface signifient, non seulement le décor, mais aussi ce que vois/pense/ressent le personnage en s'adressant à celui qui regarde. Il y a comme deux catégories d'acteur auxquels on se fie : les personnages et les interfaces. Les premiers expriment ce qu'ils ressentent et les seconds proposent des codes pour les exprimer. J'ai l'impression que notre confiance envers les interfaces dépasse celle envers le texte des personnages.

6. [Je dois tout d’abord m’excuser… - Joana Hadjithomas & Khalil Joreige, 2014](https://www.v
illa-arson.org/2014/08/je-dois-tout-dabord-mexcuser-i-must-first-apologise-joana-hadjithomas-khalil-joreige/)

Ces deux artistes s'intéressent aux arnaques par e-mails, notamment les scams. Les arnaqueurs inventent une histoire selon laquelle ils doivent transférer une grande somme d'argent mais la personne contactée doit en fournir afin de payer des frais. En contre-partie, elle est censée recevoir un pourcentage.\
Ces artistes mènent ce travail depuis 1999. Et il est étonnant de constater que ces arnaques parviennent encore à tromper : "chaque année des milliers de personnes sont abusées et plusieurs centaines de millions d’euros volés".\
Dans un entretien avec Brian Kuan Wood (dans *Internet does not exist*), les artistes expliquent l'efficacité des arnaques par le mode d'adresse individuelle utilisé. Le [témoignage de Fidel](https://www.youtube.com/watch?v=kGqu2-KC-x8) est intéressant : cet arnaqueur compare cette pratique à l'écriture d'un film où la différence réel/fiction n'est pas nette.


7. [A Brief Look at Texting and the Internet in Film - Tony Zhou, 2014](https://www.youtube.com/watch?v=uFfq2zblGXw)

Cette vidéo présente les différentes pratiques de films pour faire apparaître des échanges de messages. Ces interfaces deviennent des procédés de narration à part entière. Ces messages engendrent alors des usages et des gestes pour chercher le téléphone, pour la lecture à l'écran, pour écrire une réponse. À l'aide d'éléments graphiques, on parvient à comprendre qu'il y a conversation. L'aspect graphique fait parfois dater le film à une époque bien particulière. Mais dans la série *Sherlock*, seul un texte typographique est utilisé, et pourtant la compréhension de la réception d'un message reste efficace.


8. [Matrix - Les Wachowski, 1999](http://www.youtube.com/watch?v=6IDT3MpSCKI)

Au début du film Neo sommeille devant son écran. Il est réveillé quand son ordinateur se met à lui parler/écrire. Le passage d'un écran qui défile des documents à un écran qui affiche un message suffit à réveiller le personnage et à instaurer une tension dans le scénario.

________________

**Narration**

Ces projets mettent en place des procédés pour faire comprendre qu'il y a conversation. Elle peut être fictive ou réelle, on peut en être spectateur ou alors elle nous est destinée.
Le numérique a produit des usages et a demandé de produire des moyens de faire comprendre que quelqu'un parle (les blocs d'une conversation SMS) ou que l'on peut parler ici (un formulaire). C'est donc un ensemble d'éléments fonctionnels et de codes/usages qui sont utilisés. Le déplacement de ces projets est d'utiliser ces modalités en tant qu'éléments de narration à part entière, au-delà de leur rôle intrinsèque de communication. Ils deviennent les acteurs, le montage et le décor d'une narration.\
Ces procédés techniques portent alors un potentiel narratif et émotionnel. Les codes numériques pour faire comprendre qu'il y a conversation apparaissent comme des simulacres, des moyens pour une simulation dans ces procédés narratifs.

**Efficacité**

Je m'interroge sur l'efficacité et la crédulité que nous avons face à ces dispositifs. Dans 1., 2., 3. et 7. on se prête au jeu même si on comprend l'artifice. Mais dans 5., 6. et 8., l'effet dépasse celui du simple artifice. C'est si on adhérait (dans 6., certains adhèrent complètement en payant)\
L'efficacité du procédé tient notamment au fait que ce vocabulaire formel et d'interaction nous est familier. Malgré l'abondance de détails (comme dans 5.), on parvient à lire un récit. Ces modes de narration exercent même un fort effet d'attraction sur le lecteur. C'est le cas de Oui/non, conversation captive (2.), qui cherche à rendre captif son lecteur. On ne peut pas répondre et pourtant ce site web nous "parle". Cela évoque le silence du petit ami dans 3. ou la disparition de Facebook de la petite amie de Noah (5., elle l'a bloqué). Ou encore la tension des messages laconiques sur l'écran dans 8.. Grâce à cette familiarité visuelle et l'effet d'attraction qui en découle, ces narrations parviennent à créer une sensation de captivité, d'attente, et même d'angoisse.\
Nos interfaces craignent le silence, elles cherchent à être explicites, à nous donner des informations ou à être remplies. Le point de vue du lecteur qui ne sait pas exactement ce qui se passe, ni qui parle, et qui ne peut pas intervenir est différent de celui de l'utilisateur auquel il est habitué.

**Instantanéité**

On comprend que quelqu'un parle et on a l'impression qu'il parle à l'instant. C'est curieux, il y a comme une sensation d'oralité à lire ces échanges écrits. (dans 8., on dirait presque que le texte qui apparaît "fait un son" plus fort que ce que Néo écoute pour attirer son attention).\
En effet, cette mise en forme de la conversation est aussi temporelle, séquencée. Dans 2. et 3. elle figée et enregistrée et pourtant elle produit un effet d'instantanéité. Dans 3., les indications temporelles ouvertes ("tomorrow", "next month"), les questions, l'interaction au clic sont des éléments participants de cette instantanéité.\
Dans 8., l'apparition du texte lettres après lettres est un moyen de produire cet effet. Cependant, l'interface écran noir et lettres vertes en monospace paraît daté. Il y a comme un décalage graphique car l'interface ne s'adresse pas à nous dans notre temps contemporain. L'effet d'instantanéité semble alors désuet.\
On regarde aujourd'hui Noah (5.) sans gênes et difficultés mais au vue des rapides mises à jour des interfaces, ce court métrage risque devenir rapidement daté et obsolète.

**Point de vue**

Ces modes de narration ne varient pas leur point de vue. Il est pratique d'utiliser un vocabulaire de cinéma pour les décrire. Les desktop films (4., 5., 8.) utilisent un point de vue subjectif : on voit ce que voit le personnage principal. Les textes qui s'écrivent devant nos yeux sont comme des regards caméras. Ils s'adressent à nous en tant que lecteur.\
Dans 1., 2., 3., 6., le point de vue est aussi subjectif, mais il n'y a pas de "caméra", c'est l'écran du lecteur qui en fait office.\
Dans 7., le point de vue s'apparente plus à un point de vue omniscient. La caméra est capable de lire à distance le contenu d'un message et de le faire apparaître. C'est particulièrement le cas dans la série Sherlock, notamment à 1min06 dans la vidéo où plusieurs personnages reçoivent un même message. C'est comme si on pouvait lire un ensemble d'écran en un seul coup d'œil.\
Et pour finir, il paraît fécond de parler de double énonciation en considérant l'interface comme un personnage et un décor capable de transmettre des émotions.
