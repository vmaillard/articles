% Quand l'interface nous parle
% Vincent Maillard
% 2019

≈ 20 000 signes

À l'ouverture du film *Matrix* (Les Wachowski, 1999), Neo --\ le
personnage principal\ -- est endormi et affalé sur son bureau tout
encombré de matériel informatique. En fond sonore\ : la musique de son
casque audio, à un volume suffisamment important pour que le son soit
audible. Le seul élément en mouvement est son écran qui effectue
automatiquement des recherches dans des articles de journaux.

Soudain, ce processus s'interrompt pour laisser place à un écran noir où
s'inscrivent, une à une, les lettres qui composent la phrase\ : «\ Wake up
Neo…\ » [^1]. Cette apparition soudaine tire Neo de son sommeil. Il se
redresse, enlève son casque, à mesure que de nouveaux messages
s'écrivent sous ses yeux ébahis. Ces quelques petites lettres ont suffi
à le réveiller et à installer une tension et du suspens dans le
scénario. Le message transmis a fait preuve d'une grande efficacité, en
prenant le contrôle de l'ordinateur, en réveillant Neo et en prouvant
son omniscience\ : il prédit l'arrivée de coups sur la porte de
l'appartement.

Cette séquence oblige Neo et le regardeur à imaginer un autre qui est
ailleurs et pourtant simultanément ici. Qui parle\ ? Où est-il\ ? Est-ce
vrai\ ? Comment fait-il\ ?

![](img/matrix/1.png)

*Matrix*, Les Wachowski, 1999.

Dans cette première scène de *Matrix*, on est témoin de deux conceptions
du numérique. L'ordinateur de Neo passe d'une machine utilisée pour sa
puissance calculatoire --\ qui cherche automatiquement des données dans
des journaux\ -- à un moyen de communication qui transmet des messages
instantanément. Cette transition correspond à une évolution dans
l'histoire de l'informatique. L'artiste Casey Alt décrit ce changement
dans l'article «\ Objects of our affection\ » [^2]. Il met ainsi en
évidence deux conceptions du numérique. D'une part, la métaphore d'une
machine à calculer universelle, d'autre part, la métaphore du *média*.
Pour lui, avant 1960, la mesure du progrès informatique est basée sur la
vitesse de calcul, toutefois par la suite c'est la métaphore du média
qui prime. Une machine numérique, et cela est valide au niveau de
l'écriture des programmes par le biais de la programmation orientée
objet, est une machine capable de stocker et de manipuler des données
pour les mettre en relation. C'est cette caractéristique de média qui
m'intéresse ici.

L'ordinateur, le téléphone sont à considérer comme des médias. Pour nous
permettre de communiquer, nous utilisons des interfaces graphiques qui
sont autant des moyens techniques d'accès à des échanges que des moyens
graphiques de les signifier. Ainsi, il existe des usages et des codes
pour décrire les échanges numériques et ils nous sont familiers. Par
exemple, pour signifier la possibilité d'écrire, la barre clignotante en
est l'élément le plus simple. Pour signifier l'arrivée de messages, il
existe des systèmes de notifications graphiques, auditifs et de
vibration. Filets, couleurs, noms de contact, photographies de profil
sont alors mis en place pour signifier l'identité du correspondant et
les séparations entre les différentes étapes d'une conversation.

## Les interfaces de message dans la fiction

Dans cette contribution, j'étudierai l'usage d'interfaces graphiques et
plus précisément d'interfaces d'échange de messages dans des œuvres de
fictions. Voir des échanges numériques dans ces œuvres change de notre
rapport habituel avec elles. En effet, on regarde passivement des
interfaces qui par définition invitent à être manipulées. Notre rapport
à celles-ci n'est plus alors essentiellement fonctionnel\ : il devient
fictionnel. L'enjeu est de décrire des œuvres où les interfaces se
constituent en tant que décors, acteurs, ressorts narratifs et principes
actifs. Je m'intéresserai précisément aux notions de narrativité, de
crédulité, d'instantanéité et de point de vue.

L'exposition *Interface Culture* [^3], dont le commissariat est réalisé
par Nicolas Nova, invite à s'interroger sur les influences réciproques
entre science fiction et design d'interface. Le cheminement est
semblable ici afin de décrire des travaux où le fonctionnel échange avec
son voisin fictionnel et inversement. Étant donné que ces œuvres sont
narratives, du vocabulaire relevant du théâtre, du cinéma et de la
narratologie sera employé pour décrire l'usage d'interfaces.

## Le court métrage *Noah*

Je vais tout d'abord m'intéresser à *Noah*, un court-métrage réalisé par
Patrick Cederberg en 2013. Il raconte la rupture toute numérique de Noah
et Amy, sa copine. La principale caractéristique de ce film est le point
de vue adopté. C'est un *desktop film*\ : il est «\ filmé\ » à l'aide la
fonctionnalité de capture d'écran, donc sans caméras. Le point de vue
n'est pas pour autant statique car on ne suit pas seulement l'affichage
d'un écran mais plutôt le regard frénétique du personnage principal.
Noah réalise simultanément un grand nombre de tâches\ : choisir une
musique, discuter sur Skype, sur Facebook, faire des requêtes Google. Si
bien que la narration en devient difficile à suivre et surtout
anxiogène. On assiste à une prolifération d'interfaces, de messages de
toutes sortes, et l'on est forcé malgré tout de suivre le regard de Noah
par des zooms et des zooms inverses, en grand écart constant.

![](img/noah/2.png)

*Noah*, Patrick Cederberg, 2013.

Le récit se déroule alors grâce à une double énonciation\ : les acteurs
(Noah, sa copine, leurs amis, les rencontres fortuites) jouent leur
rôle, expriment des émotions et, dans un temps simultané, les interfaces
véhiculent du sens et des contextes. La narration utilise pour son
propre compte les éléments intrinsèques aux interfaces des réseaux
sociaux qui permettent d'exprimer des émotions (bouton «\ j'aime\ », avec
ces déclinaisons sur Facebook) et même des sentiments (avec la
possibilité de renseigner sa situation amoureuse). De fait, dans *Noah*,
les interfaces titrent et sous-titrent toutes les actions et réactions
des personnages. Le récit est ainsi immersif et particulièrement
efficace pour construire un processus d'identification. Nous sommes
familiers de ces interfaces (couleurs, typographies, arborescences,
sons, etc), si bien que le regardeur saisit aisément la situation car il
en connaît les ressorts techniques. Mais au-delà de cette facilité de
compréhension, cette sensation de familiarité cause un trouble dans la
fiction. En effet, l'interface est comme garante d'une réalité
fonctionnelle. Elle souligne le contexte crédible de cette fiction. À
mesure de la progression du récit, on est amené à se demander\ : «\ est-ce
que ça se passe réellement\ ?\ » ou alors, de façon moins dupe, à imaginer
le travail de montage derrière.

L'idée que je souhaite soutenir ici est que les interfaces ne sont pas
alors seulement des décors mais également des acteurs et des narrateurs
de l'histoire. Leurs fonctionnalités agissent en tant que discours. Et
leur discours apparaît plus sûr ou plus difficile à jouer que celui des
acteurs, comme si notre confiance envers les interfaces dépassait celle
envers le texte des personnages. Leur efficacité à signifier des
échanges montre la crédulité que nous avons face à elles. Nous sommes
crédules lorsque Noah publie un message sur un fil Facebook car nous
imaginons le contexte de ses relations susceptibles alors de le lire.
Nous imaginons également le hasard impossible à anticiper des rencontres
lorsqu'il se connecte à Chatroulette [^4], encore une fois, en mettant
de côté le montage. Ces séquences poussent à imaginer le contexte
crédible et non fictionnel propre au web actuel. Cette situation
rappelle l'expression «\ ici et là\ » que Pierre-Damien Huyghe emploie
pour désigner nos expériences numériques. [^5] Les actions de Noah qui
adviennent devant nos yeux et qui s'expriment par l'intermédiaire
d'interfaces sont le résultat d'un *ici* et d'un *là*, un contexte
indéfini, imprévisible et incontrôlable.

Ce potentiel de crédibilité des interfaces devient un ressort narratif.
C'est particulièrement le cas lorsque Noah fait des choix contraires au
bon sens. Le regardeur prend alors parti contre lui mais la narration se
poursuit selon l'impulsivité du personnage. Ces situations produisent un
véritable malaise. On est tenu en témoin de la violation d'une intimité
numérique, et d'actes inconscients qui aggravent la situation.
L'impulsivité et l'hésitation du personnage sont traduites par son
regard qui parcourt l'écran par saccades et sans jamais se fixer, par sa
souris qui tourne autour des boutons «\ Sauvegarder\ » ou «\ Envoyer\ ».
C'est donc exclusivement l'interface et le point de vue adopté qui
expriment les pensées du personnage quand, par ses actes, il s'isole.

La narration à l'aide d'interfaces produit, en plus d'un potentiel de
crédulité, un effet d'instantanéité. Les interfaces s'entourent
d'éléments pour signifier que l'on interagit avec elles en temps réel.
Ces précisions temporelles modifient leur réception dans le temps
différé de celui des œuvres que nous étudions. Dans la séquence
d'ouverture de *Matrix*, les lettres apparaissent les unes après les
autres. Dans *Noah*, l'instantanéité passe par les indications «\ en
train d'écrire\ », la présence des champs de saisie dans lequel Noah
écrit progressivement, les notifications, les réactions de relations à
distance. Cependant, l'interface de *Matrix* composée d'un écran noir
et de lettres vertes en monospace paraît datée. Il y a comme un décalage
graphique car l'interface ne s'adresse pas à nous dans notre temps
contemporain. L'effet d'instantanéité semble alors désuet. À
l'inverse, les interfaces utilisées dans *Noah* nous sont
contemporaines. Elles conservent alors leur sensation de temps réel qui
joue en faveur de l'effet d'attraction et du procédé d'identification du
regardeur. Il en résulte l'impression que la narration s'écrit à
l'instant. Cette particularité est notamment induite par l'utilisation
de l'écran du regardeur comme la limite ou le cadre de la narration. Si
l'on regarde *Noah* sur un écran d'ordinateur, le point de vue adopté
agit comme une simulation de l'activité de l'ordinateur. [^6] Le récit
débute notamment lorsque Noah rentre son mot de passe et s'achève quand
il choisit d'éteindre son ordinateur. Ainsi, au-delà d'être le moyen
technique destiné à l'affichage, l'écran du regardeur devient un élément
actif de la narration.

## La série *Sherlock*

Cette caractéristique évoque une remarque de la chercheuse Michele
Tepper à propos de la série *Sherlock* (BBC, 2010), en effet, elle
décrit le procédé d'affichage des messages comme «\ s'emparant de
l'écran du regardeur en tant que partie même du récit.\ » [^7] Elle est
citée par Tessa Dwyer dans un article intitulé «\ From Subtitles to SMS\ »
[^8], l'auteur montre la relation qui existe entre ces éléments textuels
et les sous-titres en convoquant des publications sur l'usage d'éléments
textuels dans des films et des études sur l'oculométrie (l'étude des
mouvements de l'œil, en anglais *eye tracking*).

Dans la série *Sherlock*, des éléments textuels apparaissent à de
nombreuses reprises sur l'image. Il y a deux fonctions distinctes à ces
interventions\ : celle de représenter des échanges numériques et celle de
représenter les indices repérés par le détective. [^9] Le procédé
consistant à montrer des échanges de messages numériques à même l'image
a été utilisé à de nombreuses reprises, notamment dans les séries *House
of Cards* (2013), *Glee* (2013) ou encore dans des films comme
*Disconnect* (2012) ou *Non-Stop* (2014). Une des particularités
notables de la série *Sherlock* est leur apparition sans autre élément
graphique que la typographie\ : ni cadres, ni couleurs. Le procédé reste
pourtant compréhensible, d'autant plus qu'il est lié au geste d'écrire
ou de regarder un téléphone. Avec l'apparition typographique des
indices, c'est une métaphore efficace pour décrire la puissance de
réflexion et le pouvoir d'observation du héros. Sherlock écrit
extrêmement vite, à plusieurs personnes à la fois, en cachant des
messages aux autres pour avoir un temps d'avance ; il calcule, réordonne
et parcourt nombres et lettres. En rendant ces processus d'échanges et
de réflexions visibles, le rythme du récit est enlevé, et surtout le
regardeur est invité à rentrer dans la confidence des pensées de
Sherlock.

Le premier épisode de la série, intitulé *Une étude en rose* (*A Study
in Pink*), est riche d'exemples. Les messages apparaissent
progressivement à l'écran et déclenchent les notifications habituelles
(sons, vibrations). Ils sont spatialisés\ : leur position correspond à
l'emplacement d'un téléphone et leur taille diminue dans la profondeur
du plan. Enfin, au bout de quelques secondes, ils disparaissent. Ce
procédé permet une démultiplication du regard. Le point de vue de la
caméra a beau être face aux acteurs, le regardeur obtient la capacité
omnisciente de lire ce qu'ils voient sur leur téléphone. C'est d'autant
plus visible dans une scène où une dizaine de journalistes reçoivent un
même message instantanément.

![](img/sherlock/sherlock-wrong.jpg)

*Sherlock*, saison 1, épisode 1, Paul McGuigan, 2010.

Le procédé d'apparition des indices est semblable à celui des messages.
Il faut noter une particularité cependant. En effet, de nouveau dans le
premier épisode, lors d'un contre-champ sur Sherlock, le texte apparaît
en miroir, comme s'il était en fait vu par le détective. Ce changement
de vision narrative déplace la fonction purement informative des
apparitions textuelles, elles deviennent des éléments fictionnels et
narratifs.

![](img/sherlock/4.jpeg)

*Sherlock*, saison 1, épisode 1, Paul McGuigan, 2010.

À ce propos, Carol O'Sullivan dans son livre *Translating Popular Film*,
convoque le principe de *métalepse*. Ce procédé narratif, qui
s'apparente à la mise en abyme, correspond à un déplacement de la
fiction sortant de son champ narratif pour se mêler à la réalité [^10].
Gérard Genette analyse ce procédé comme un «\ passage d'un niveau
narratif à un autre\ » [^11], il ajoute également\ : «\ la conduite
métaleptique a pour motif \[…\] une **illusion** consistant à recevoir
la fiction comme une réalité, et pour contenu le franchissement
illusoire de la frontière qui les sépare\ » [^12]. Les éléments textuels
dans la série *Sherlock* dépasse la nature du sous-titre quand ils font
osciller le point de vue entre celui du regardeur et celui du
personnage. Ce procédé s'apparente à une métalepse et brouille ainsi la
réception de la fiction. Finalement, on peut dire que ce procédé
narratif est également utilisé dans *Noah*. En effet, quand Noah fait
appel à un réseau d'amis sur Facebook ou à des inconnus sur
Chatroulette, la fiction est déplacée. La connaissance du fonctionnement
de ces interfaces agit comme une métalepse et change le registre
narratif de la fiction. Le regardeur doute sur le fait qu'il regarde
véritablement une fiction ; cependant, Genette nuance la réception d'une
métalepse, elle «\ ne peut guère attendre une pleine et entière
suspension d'incrédulité, mais seulement une simulation ludique de
crédulité.\ » [^13]

Par rapport à la relation aux sous-titres, l'article «\ From Subtitles to
SMS\ » de Tessa Dwyer est éclairant. Elle compare en effet les résultats
de différentes études d'*eye tracking*. J'en retiendrai ici les
conclusions. En premier lieu, ces textes sont de natures très
différentes\ : contrairement à un texte imprimé, les sous-titres
apparaissent pendant un laps de temps court et sur un fond en mouvement.
Leur lecture en devient bien particulière. On note plus d'allers-retours
et un temps de fixation inférieur à la normale, si bien que les
mouvements habituels de l'œil pour la lecture -- les saccades et les
fixations -- sont moins différenciés. C'est particulièrement le cas pour
les sous-titres constitués d'une seule ligne. Ces spécificités
s'expliquent notamment par la redondance des informations induites par
les sous-titres, par le fait de revenir à un sous-titre pour vérifier
s'il n'a pas changé. Dwyer note alors que les sous-titres brouillent les
frontières entre l'acte de lire et celui de regarder. C'est d'autant
plus le cas dans la série *Sherlock* où les éléments textuels
apparaissent en relation directe avec des éléments visuels. Notre œil
parcourt plus qu'il ne les lit. Ce qui n'est pas sans rappeler les
scènes où Noah parcourt son écran de manière impulsive. En extrapolant,
on peut avancer que notre rapport aux interfaces est certainement
semblable et qu'il est donc constitué d'une prise en compte morcelée et
réduite d'un ensemble ainsi que d'allers-retours rapides de notre œil
entre des éléments familiers qu'on parcourt et des éléments de contenus
qu'on lit.

## La fiction des *spams*

En poursuivant dans un contexte relevant moins de la fiction et plus du
quotidien, la crédulité que nous avons face aux interfaces est manifeste
lorsque l'on reçoit spams. Si l'on pense l'interface comme un décor,
acteur et narrateur crédible, alors les spams sont aussi à considérer
comme des objets exploitant le caractère narratif des interfaces.
L'exposition «\ Je dois tout d'abord m'excuser\ » des artistes Joana
Hadjithomas et Khalil Joreige aborde cette thématique. [^14] Ils
travaillent en particulier sur les *scams*, un type particulier de spam
où les arnaqueurs inventent une histoire selon laquelle ils ont une
grande somme d'argent à transférer cependant, la personne contactée
doit fournir de l'argent afin de payer des frais. En contre-partie, elle
est censée recevoir un pourcentage. Hadjithomas et Joreige collectent
les traces de ces *scams* depuis 1999 et ils comptabilisent des milliers
de personnes abusés chaque année. Le procédé est donc efficace. Les
arnaqueurs comparent leur travail à la préparation d'un film\ : ils
choisissent des personnages, un contexte, des accessoires et
construisent une histoire. Le potentiel de crédibilité est garanti à la
fois par la précision de ces détails et par les interfaces utilisées.
Ces arnaqueurs envoient des mails individuels et répondent en s'adaptant
à la personne. Ce simulacre d'une adresse personnelle crée une
identification du récepteur à l'envoyeur. Considérer ces *scams* comme
des récits fictionnels permet de voir dans quelles mesures ils relèvent
de la métalepse. Ces arnaques exploitent en effet le potentiel de ce
procédé narratif en incorporant dans leur récit fictif des éléments, des
détails et des personnalités réels. Elles se servent également de la
métalepse que peut offrir l'interface\ : celle de faire que lecteur
prenne part au récit que les arnaqueurs ont entamé. Par un procédé
narratif et un procédé technique qui devient un potentiel de
narrativité, le récit fictionnel des arnaqueurs est transformer en un
mail crédible, susceptible d'engager une conversation.

Ainsi, l'interface parvient à duper. La familiarité avec elle n'invite
pas à la remettre en cause. Les œuvres de fictions citées plus haut
participent d'un déplacement d'éléments fonctionnels à des éléments
narratifs et fictionnels. Elles mettent également en évidence le fort
potentiel d'adhésion qu'implique les interfaces. Il est intéressant
alors de s'interroger non seulement sur des problématiques techniques et
fonctionnelles mais également sur le pouvoir narratif des interfaces
ainsi que leur potentiel d'adhésion et la nature de leur perception.

[^1]: «\ Réveille-toi Neo…\ »

[^2]: Casey Alt, «\ Objects of our affection -- How Object Orientation
    Made Computers a Medium\ », *Media archaeology*, 2006.

[^3]: Exposition à la Cité du design (Saint-Étienne) du 3 novembre 2015
    au 14 août 2016.

[^4]: Site web de messagerie instantanée mettant les utilisateurs en
    relation de manière aléatoire.

[^5]: Notamment dans la vidéo «\ Pierre-Damien Huyghe : qu'est-ce que le
    multi-écran ?\ », réalisée par l'organisme *Designers interactifs*.
    URL\ : bit.ly/2kWTyds

[^6]: C'est d'autant plus le cas si l'on utilise des ordinateurs de
    marque Apple, en adéquation avec le système d'exploitation utilisé
    par Noah.

[^7]: Michele Tepper, « The Case of the Travelling Text Message », 2011\ : « this is capturing the viewer's screen as part of the narrative
    itself. »

[^8]: Tessa Dwyer, «\ From Subtitles to SMS: Eye Tracking, Texting and
    Sherlock\ », *Refractory: A Journal of Entertainment Media*, 2015.

[^9]: Deux typographies sont utilisées, respectivement\ : l'*AF
    Generation Z* et la *P22 London Underground*.

[^10]: Par exemple par l'apparition de l'auteur dans sa fiction, par des
    récits enchâssés ou par l'adresse au lecteur.

[^11]: Gérard Genette, *Figures III*, p. 243.

[^12]: Gérard Genette, *Métalepse*, p. 51.

[^13]: Gérard Genette, *Métalepse*, p. 25.

[^14]: «\ Je dois tout d'abord m'excuser…\ », exposition de Joana
    Hadjithomas et Khalil Joreige à la Villa Arson, Nice, 2014.
