
# Grammaire générale et raisonnée

1660 :
p. 68

"Il y a encore un autre pronom, qu'on appelle Relatif, qui quæ, quod, lequel, laquelle.
Ce pronom relatif a quelque chose de commun avec les autres pronoms, & quelque chose de propre.
Ce qu'il a de commun, est qu'il se met au lieu du nom, & plus généralement mesme que tous les autres pronoms, se mettant pour toutes les personnes. Moy QUI suis Chrestien : Vous QUI estes Chrestien : Lui QUI est Roy.
Ce qu'il a de propre est que la proposition dans laquelle il entre (qu'on peut appeler incidente) peut faire partie du sujet, ou l'attribué d'une autre proposition, qu'on peut appeler principale.
On peut entendre cecy, qu'on ne se souvienne de ce que nous aurons dit dès le commencement de ce discours"


1754 :
p. 114
"Il y a encore un autre pronom, qu'on appelle Relatif, qui quæ, quod, lequel, laquelle.
Ce pronom relatif a quelque chose de commun avec les autres pronoms, & quelque chose de propre.
Ce qu'il a de commun, est qu'il se met au lieu du nom, & plus généralement même que tous les autres pronoms, se mettant pour toutes les personnes. Moi QUI suis Chrétien : Vous QUI êtes Chrétien : Lui QUI est Roi.
Ce qu'il a de propre peut être considéré en deux manières.
La I. en ce qu'il a toûjours rapport à un autre nom ou pronom, qu'on appelle antécédent, comme : Dieu qui est saint. Dieu est l'antécédent du relatif qui. Mais cet antécédent est quelque fois sous-tendu & non exprimé ; sur-tout dans la Langue Latine, comme on a fait voir dans la Nouvelle Méthode pour cette Langue.
La 2e chose est que le Relatif a de propre, & je ne sache point avoir été remarqué par personne, est que la proposition dans laquelle il entre, (qu'on peut appeler incidente) peut faire partie du sujet ou de l'attribut d'une autre proposition, qu'on peut appeler principale.
On peut entendre ceci, qu'on ne se souvienne de ce que nous aurons dit dès le commencement de ce discours"


Kirby 
"Let's be honest: who reads the docs? Right? It's totally fine to explore your Kirby installation on your own without our help. If you don't want to miss out on any of the possibilities though, the docs are always here to help."





Recette / Partition / Documentation programme / Grammaire


Claire Mathieu - L’algorithmique
Algorithme loyal : qui fait ce qu’on lui demande de faire


Abott
Parse



https://www.franceculture.fr/emissions/avis-critique/le-francais-est-a-nous-de-m-candea-et-l-veron-nous-sommes-tous-des-femmes-savantes-de-l-naccache
10’ : tension entre linguiste/anarchiste et grammairien/conservateur
20’ : grammaire implicite


https://fr.wikipedia.org/wiki/Rapport_Gr%C3%A9goire