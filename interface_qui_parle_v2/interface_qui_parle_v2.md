Quand l'interface nous parle
============================

**Vincent**

≈ 19 000 signes

**Abstract**

Je m'intéresse ici à des interfaces graphiques mettant en scène
l'échange de messages. Je m'interroge sur le potentiel de narrativité et
d'illusion de ces interfaces : comment elles racontent et agissent en
même temps sur les échanges, comment elles peuvent engager le receveur,
comment elles mettent en scène un ailleurs.

------------------------------------------------------------------------

À l'ouverture du film *Matrix* (Les Wachowski, 1999), Neo - le
personnage principal - est endormi sur son bureau tout encombré de
matériel informatique. Sur ses oreilles, son casque audio joue à un
volume suffisamment important pour que le son soit audible dans la
pièce. Le seul élément en mouvement est son écran d'ordinateur : on voit
la machine effectuer des recherches automatiques dans des articles de
journaux.

Ce processus s'interrompt soudainement pour laisser place à un écran
noir où s'inscrivent, une à une, chacune des lettres qui composent la
phrase : « Wake up Neo... » [^1]. Cette apparition le tire de son
sommeil. Il ouvre les yeux, se redresse, enlève son casque, et sa
stupeur grandit à mesure que de nouveaux messages s'écrivent sous ses
yeux ébahis, allant jusqu'à prédire l'arrivée de coups sur la porte de
son appartement.

Cette suite de message agit comme une conversation instantanée et comme
une série d'instructions que Neo, voir le film même, exécute. Elle est à
la fois le script de l'histoire et un personnage y prenant part. C'est
une trace écrite qui dans un même temps décrit et agit sur l'action tout
en dépassant les contraintes spatiales. Elle invite à imaginer un autre
qui est ailleurs et pourtant simultanément ici.

![*Matrix*, Les Wachowski, 1999.](img/matrix/1.png)

On peut analyser cette scène au moyen de deux conceptions du numérique.
L'ordinateur de Neo passe d'une machine utilisée pour sa puissance
calculatoire effectuant des requêtes automatiques à un moyen de
communication transmettant des messages instantanément. Cette transition
correspond à une évolution historique de l'informatique. L'artiste Casey
Alt décrit ce changement dans l'article « Objects of our affection »
[^2]. Il met ainsi en évidence deux conceptions du numérique : d'une
part, la métaphore d'une machine à calculer universelle et de l'autre,
la métaphore du *média*. Pour lui avant 1960, la mesure du progrès
informatique est basée sur la vitesse de calcul. Toutefois, par la suite
c'est la métaphore du média qui prime. Un ordinateur est alors
considérer une machine capable de stocker et de manipuler des données
pour les mettre en relation.

Les productions du Xerox PARC ont contribué à créer des interfaces
graphiques pour interagir avec ces machines, par exemple : Douglas
Engelbart présente en 1968 les principes de la souris, du traitement de
texte (The Mother of All Demos) et en 1981 le Xerox Star est publié en
intégrant une interface graphique basée sur la métaphore du bureau
(icônes, fenêtres, fichiers et dossiers). Les interfaces graphiques sont
depuis devenues nos moyens privilégiés d'interaction avec les machines
numériques.

Je vais ici m'intéresser à des interfaces graphiques mettant en scène
l'échange de messages. Plus précisément, je vais m'intéresser à des
représentations d'interfaces graphiques. Je me concentrerai ainsi sur
des œuvres de créateurs qui jouent de l'interface, et non directement
sur les interfaces elles-mêmes. Ce seront des films, des sites web, des
installations qui utilisent des interfaces au sein de leur narration. Ce
choix a pour finalité d'être en mesure de décrire des moments où l'on
regarde les interfaces d'un peu plus loin que lorsqu'on les utilise. Ils
permettront également d'utiliser du vocabulaire et des concepts
empruntés à la narratologie. Et ce, en assumant l'ambiguïté de pouvoir
parler à la fois de ce que nous donne à voir l'œuvre et de ce que
recouvre les interfaces.

L'objectif n'étant pas de faire système mais d'à partir de la
description de ces œuvres, de leurs jeux formels et conceptuels, de
proposer une approche de notre rapport aux interfaces. Approche qui ne
soit pas seulement fonctionnelle ou interactive, mais décalée des
considérations sur l'expérience utilisateur ou l'intuitivité pour plutôt
inviter à réfléchir sur leurs natures et particularités narratives, voir
fictionnelles.

De ces représentations d'interfaces d'échange de messages, je
m'intéresse à leur qualité narrative : en véhiculant des messages tout
en les programmant, elles sont décors et acteurs. Ces objets jouent de
différents niveaux de narration. Pour reprendre Genette dans *Figure
III*[^3], ils jouent sur deux mondes : "celui *où* l'on raconte, celui
*que* l'on raconte". De ce point de vue, les interfaces entretiennent
des illusions : illusions de l'instantanéité, de la neutralité du
transfert, de l'identité du destinateur ou du récepteur. On peut ainsi
relever des moments où le texte agit tel un regard caméra en rompant
avec le niveau narratif où il se trouvait.

------------------------------------------------------------------------

> *à partir d'ici, ce qui est écrit est parfois ébauché pour visualiser
> le déroulé*

------------------------------------------------------------------------

Je vais commencer par analyser un objet qui relève de la littérature
numérique sur le web. C'est à partir de sites web de littérature
numérique que j'ai commencé à relever ce qui m'intéresse ici.

**Oui/non, conversation captive (Luc Dall'Armellina, 2003)**

Consultable à cette adresse : lucdall.free.fr/oui/

-   Description :
    -   première écran : au lecteur est adressé cette question :
        "Voulez-vous converser avec quelqu'un qui vous aime d'un très
        grand amour ?", il est possible de répondre "oui" ou "non"
    -   deuxième écran : il est demandé d'entrer un prénom
    -   troisième écran : une série de 100 messages, utilisant le prénom
        renseigné, où il n'y a pas d'autres choix que de les lire. Ils
        s'adressent au lecteur, l'interpellent, le questionnent, le
        complimentent
    -   le ton des messages est ironique, plein d'emphase, il prolonge
        la promesse du "très grand amour" du premier écran

![Deux captures d'écran de *Oui/non, conversation captive*.](img/oui/1.jpg)

Ce site web s'adresse explicitement au lecteur, lui demande son prénom,
et l'emmène dans une lecture séquencée au clic, sur le ton de la
confidence. Il mime un monologue adressé au lecteur

-   la première question et le formulaire pour renseigner son prénom
    sont deux choix qui engagent le lecteur
-   le lecteur est alors impliqué dans l'histoire que raconte ce site
    web
-   s'installe une relation avec l'histoire où Luc Dall'Armellina invite
    le lecteur a suspendre sa crédulité (Coleridge : "suspension
    volontaire de la crédulité")
-   la rapidité de passage entre chaque message produit un effet
    d'instantanéité, elle ré-active des messages qui pour autant ne sont
    pas crées sur le moment, ils sont enregistrés
-   une fois démarrée, la narration est captive : il n'y a pas moyen de
    revenir en arrière, les choix du lecteur
-   ce site web cherche à créer une illusion de conversation, un
    simulacre d'adresse où le site web mène la conversation sans que
    lecteur sache exactement sur quoi elle se base ni là où elle peut
    aller

**Portrait n°1 (Luc Courchesne, 1990)**

Ces hésitations entre crédulité, illusion, programmation, spontanéité se
retrouvent dans l'œuvre "Portrait n°1" de Luc Courchesne. Publiée en
CD-ROM puis restaurée en HTML 5, cette œuvre est un portrait interactif
d'un personnage appelé Marie. [^4] Au moyen d'une souris, il est
possible de commencer à lui adresser la parole, Marie commence alors à
bouger et à regarder le regardeur, il s'ensuit une conversation qui peut
durer quelques minutes. Cette conversation s'achève lorsque Marie
prononce cette phrase : "Maintenant, si vous voulez m'excuser..."

![Luc Courchesne, *Portrait n°1*, 1990](img/portrait/1.jpg)

À la manière de "Oui/non, conversation captive", on retrouve ce jeu
autour du simulacre d'adresse. D'autant plus que dans ces deux œuvres,
il y a des clins d'œil fréquents à la nature même des interfaces qui
sont en train d'être manipulées. Ces œuvres font entrer leur lecteur
dans leur narration en lui donnant l'illusion d'une interactivité. Il
apparaît ici que ces interfaces sont propices à ouvrir leur narration et
de ce fait à produire un jeu invitant à se questionner sur la réalité de
ce qui se déroule. L'interactivité est malmenée car le lecteur sent bien
qu'il ne maîtrise pas réellement la conversation, de plus, l'illusion se
brise par la réplicabilité des conversations.

Ce jeu de conversations captives est prenant pour le lecteur :
l'utilisation du prénom, les questions intimes que les interlocuteurs
peuvent s'adresser, les regards caméras de Marie produisent une curieuse
sensation d'interactivité, l'invitent à suspendre sa crédulité. D'autant
plus, qu'on peut dire que l'interface se confond avec la personne qui
s'adresse au lecteur. Je m'interroge sur cette qualité narrative que
montre les interfaces. La retrouve-t-on dans les interfaces graphiques
que l'on pratique quotidiennement ? Est-ce que l'on se questionne sur
leur possibilité à créer de la fiction ?

**La série *Sherlock* (Paul McGuigan - BBC, 2010)**

Je change ici de registre pour parler de la mise en scène d'échanges de
messages dans des films.

Dans la série *Sherlock*, des éléments textuels apparaissent à de
nombreuses reprises sur l'image. Il y a deux fonctions distinctes à ces
interventions : celle de représenter des échanges numériques et celle de
représenter les indices repérés par le détective.

![](img/sherlock/4.jpeg)

Le premier épisode de la série, intitulé *Une étude en rose* (*A Study
in Pink*), est riche d'exemples. [^5] Tessa Dwyer, maîtresse de
conférence en analyse de film (Université Monash), aborde quelques
spécifités de *Sherlock* dans un article intitulé « From Subtitles to
SMS ». [^6] Elle revient notamment sur un moment particulier du premier
épisode où lors d'un champ-contrechamp, la prise de vue sur Sherlock
(Benedict Cumberbatch) montre une intervention textuelle telle que la
voit Sherlock et donc en miroir pour le spectateur. Ce déplacement de
point de vue questionne la nature du texte à l'écran : il est à la fois
interne et externe au récit. Dwyer cite alors le livre *Translating
Popular Film* de Carol O'Sullivan [^7] où l'autrice convoque la figure
de la *métalepse* pour définir ces transgressions de niveaux narratifs.

Ce procédé narratif, qui s'apparente à la mise en abyme, correspond à
une fiction sortant de son champ narratif [^8]. Gérard Genette analyse
ce procédé comme un « passage d'un niveau narratif à un autre » [^9],
ailleurs, il ajoute : « la conduite métaleptique a pour motif \[...\]
une *illusion* consistant à recevoir la fiction comme une réalité, et
pour contenu le franchissement illusoire de la frontière qui les
sépare » [^10]. Les éléments textuels dans la série *Sherlock* dépasse
la nature du texte à l'écran quand ils font osciller le point de vue
entre celui du regardeur et celui du personnage. Ce procédé s'apparente
à une métalepse et brouille ainsi la réception de la fiction.

------------------------------------------------------------------------

> *peut-être qu'il serait mieux de supprimer le passage sur la série
> Sherlock (qui ne parle pas à proprement parler d'échange de messages).
> Pour plutôt faire une transition entre "Portrait n°1" et "Noah". Dans
> "Portrait n°1", Marie fait allusion à "L'Invention de Morel" d'Adolfo
> Bioy Casares, roman dont les jeux narratifs font que le personnage
> principal en rencontre d'autres qui ne font pas partis de sa réalité.
> Ces différents niveaux narratifs pourraient permettre d'introduire la
> figure de la métalepse*

------------------------------------------------------------------------

***Noah* (Patrick Cederberg, 2013)**

Ce court-métrage raconte la rupture de Noah et Amy, sa copine. La
principale caractéristique de ce film est le point de vue adopté. C'est
un *desktop film* : il est « filmé » à l'aide la fonctionnalité de
capture d'écran, donc sans caméras. Le point de vue n'est pas pour
autant statique car il est subjectif et nous permet de suivre le regard
frénétique du personnage principal. Noah réalise simultanément un grand
nombre de tâches : choisir une musique, discuter sur Skype, sur
Facebook, faire des requêtes Google. Si bien que la narration, basée sur
la prolifération d'interfaces, en devient difficile à suivre et surtout
anxiogène.

![](img/noah/1.png)

Le récit se déroule grâce à une double énonciation : les acteurs (Noah,
sa copine, leurs amis, les rencontres fortuites) jouent leur rôle,
expriment des émotions et, dans un temps simultané, les interfaces
véhiculent du sens et des contextes. La narration utilise pour son
propre compte les éléments intrinsèques aux interfaces des réseaux
sociaux qui permettent d'exprimer des émotions (bouton « j'aime », avec
ces déclinaisons sur Facebook) et même des sentiments (avec la
possibilité de renseigner sa situation amoureuse).

Ainsi, dans *Noah*, les interfaces titrent et sous-titrent toutes les
actions et réactions des personnages. Elles deviennent alors comme
garantes du contexte fonctionnel de cette fiction, au point où le doute
peut s'installer chez le regardeur. À mesure de la progression du récit,
il peut être amené à se demander : « est-ce que ça s'est passé
réellement ? » ou alors, de façon moins dupe, à imaginer le travail de
montage derrière.

L'idée que je souhaite soutenir ici est que les interfaces ne sont pas
alors seulement des décors mais également des acteurs et des narrateurs
de l'histoire. Leurs fonctionnalités agissent en tant que discours. Et
leur discours apparaît plus sûr ou plus difficile à jouer que celui des
acteurs, comme si notre confiance envers les interfaces dépassait celle
envers le texte des personnages. Leur efficacité à signifier des
échanges montre la crédulité que nous avons face à elles. Nous sommes
crédules lorsque Noah publie un message sur un fil Facebook car nous
imaginons le contexte de ses relations susceptibles alors de le lire.
Nous imaginons également le hasard impossible à anticiper des rencontres
lorsqu'il se connecte à Chatroulette [^11], encore une fois, en mettant
de côté le montage. Ces séquences poussent à imaginer le contexte
crédible et non fictionnel propre au web actuel. Cette situation
rappelle l'expression « ici et là » que Pierre-Damien Huyghe emploie
pour désigner nos expériences numériques. [^12] Les actions de Noah qui
adviennent devant nos yeux et qui s'expriment par l'intermédiaire
d'interfaces sont le résultat d'un *ici* et d'un *là*, un contexte
indéfini, imprévisible et incontrôlable.

*Noah* se joue ainsi de différents niveaux narratifs. La connaissance du
fonctionnement de ces interfaces agit comme une métalepse et change le
registre narratif de la fiction. Le regardeur est en position de se
demander si tout relève bien de la fiction. Sur la réception d'une
métalepse, Genette écrit qu'elle « ne peut guère attendre une pleine et
entière suspension d'incrédulité, mais seulement une simulation ludique
de crédulité. » [^13]

**Spams**

Cette suspension d'incrédulité affleure lorsque l'on reçoit un spam. Si
l'on pense l'interface comme un décor, acteur et narrateur crédible,
alors les spams sont aussi à considérer comme des objets exploitant le
caractère narratif des interfaces.

Les artistes Joana Hadjithomas et Khalil Joreige travaillent depuis 1999
à une collecte de spams et ils comptabilisent des milliers de personnes
abusés chaque année pour plusieurs centaines de millions d'euros volés.
Ce travail a abouti à l'exposition « Je dois tout d'abord m'excuser » et
au livre *Les Rumeurs du monde*. [^14] Ils se sont concentrés sur les
*scams*. [^15] Dans ces *scams*, les arnaqueurs comparent leur travail à
la préparation d'un film : ils choisissent des personnages, un contexte,
des accessoires et construisent une histoire.

Ce sont donc des récits fictionnels qui s'inspirent de faits réels et
dont le but est d'engager le receveur dans leur récit. On peut dire que
ce procédé relève de la métalepse : l'arnaqueur cherche à faire rentrer
le monde du lecteur dans le monde de son récit. Le potentiel de
crédibilité est soutenu à la fois par la précision de ces détails et par
les interfaces utilisées. L'arnaqueur se sert ainsi d'un procédé
narratif (incorporation d'éléments réels dans sa fiction) et d'un
procédé fictionnel (l'interface graphique comme décor ) pour produire un
récit capable de transgresser son propre niveau narratif.

------------------------------------------------------------------------

Pour décrire la méthode de travail de Hadjithomas et Joreige, Laura U.
Marks, professeure à l'université Simon Fraser, reprend leurs termes :
"\[ils\] 'plantent le cadre' et le laissent ouvert pour que la réalité
externe y laisse une empreinte, pour que 'les événements s'insèrent dans
le cadre'". [^16] On peut dire que les interfaces graphiques
fonctionnent de manière analogue : elles plantent un cadre et laissent
un ailleurs y pénétrer. Ce procédé interactif possède en soit un fort
pouvoir d'illusion. Il est un procédé narratif qui ne s'annonce pas
comme tel.

Les objets analysés ici montrent comment les interfaces graphiques
permettent, soutiennent et agissent sur la narration. Leur réception
n'est pas sans rappeler celle que décrit Tzetan Todorov à propos d'un
récit fantastique. Todorov parle d'une "hésitation" : "si l'hésitation
du lecteur \[...\] s'est dissipée, alors l'ouvrage appartient à la
catégorie qui vise au 'merveilleux'. Dans la situation contraire, si
l'hésitation persiste, alors l'histoire appartient à la catégorie
'd'inquiétante étrangeté'". [^17] Cette analogie n'est cependant pas
valide à propos d'une conversation débutée à partir d'un *scam* : en
effet il y a engagement du lecteur, pouvant aller jusqu'à l'envoi
d'argent. On s'éloigne ainsi de la "simulation ludique de crédulité"
dont parlait Genette. Le pouvoir illusionniste des interfaces graphiques
paraît être fondée sur une métalepse potentielle qui soutient
l'engagement du lecteur, et elle peut fonctionner même dans le cas d'un
récit fictif.

[^1]: « Réveille-toi Neo... »

[^2]: Casey Alt, « Objects of our affection - How Object Orientation
    Made Computers a Medium », *Media archaeology*, 2006.

[^3]: p.245

[^4]: Elle est consultable ici :
    https://www.fondation-langlois.org/Portrait-no-1/

[^5]: *Sherlock*, saison 1, épisode 1, Paul McGuigan, 2010.

[^6]: Tessa Dwyer, « From Subtitles to SMS: Eye Tracking, Texting and
    Sherlock », *Refractory: A Journal of Entertainment Media*, 2015.

[^7]: O'Sullivan, Carol. 2011. *Translating Popular Film*. Basingstoke
    and New York: Palgrave Macmillan.

[^8]: Par exemple par l'apparition de l'auteur dans sa fiction, par des
    récits enchâssés ou par l'adresse au lecteur.

[^9]: Gérard Genette, *Figures III*, p. 243.

[^10]: Gérard Genette, *Métalepse*, p. 51.

[^11]: Site web de messagerie instantanée mettant les utilisateurs en
    relation de manière aléatoire.

[^12]: Notamment dans la vidéo « Pierre-Damien Huyghe : qu'est-ce que le
    multi-écran ? », réalisée par l'organisme *Designers interactifs*.
    URL : bit.ly/2kWTyds

[^13]: Gérard Genette, *Métalepse*, p. 25.

[^14]: « Je dois tout d'abord m'excuser... », exposition de Joana
    Hadjithomas et Khalil Joreige à la Villa Arson, Nice, 2014 et Joana
    Hadjithomas, Khalil Joreige, *Les Rumeurs du monde*, Sternberg
    Press, Villa Arson, 2015.

[^15]: Un type particulier de spam où les arnaqueurs inventent une
    histoire selon laquelle ils ont une grande somme d'argent à
    transférer cependant, la personne contactée doit fournir de l'argent
    afin de payer des frais. En contre-partie, elle est censée recevoir
    un pourcentage

[^16]: Laura U. Marks, "Très chers, mes plus sincères salutations,
    comment allez-vous ?", *Les Rumeurs du monde*, p.180.

[^17]: Rasha Salti, "Sans toi, je ne suis rien"*Les Rumeurs du monde*,
    p.217.