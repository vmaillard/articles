## Sommaire

- ajax_et_innovation :
Analogie entre la requête AJAX et l'innovation

- entretien_f_bon :
Entretien avec François Bon lors de l'écriture du mémoire de DNSEP

- interface_qui_parle :
Exploration du potentiel narratif de l'usage d'interface dans la fiction

- workshop_decembre :
Bilan du workshop porté avec Émile

---

pandoc -c pandoc.css -s {in}.md -o {out}.html