

https://www.quaternum.net/2021/10/31/les-formats-entre-fondements-de-ledition-et-nouveau-cheminement-litteraire/#cramer_language_2008

https://accra-recherche.unistra.fr/laccra/membres/doctorants/claire-davril/

http://pierredamienhuyghe.fr/documents/audio/141014_L'art%20comme%20milieu%20m%C3%A9thodique.mp3


https://www.unilim.fr/interfaces-numeriques/4542

"Or, selon le programmeur et designer Allan Cooper, il y a un « mythe de la métaphore » (Cooper 1995), qui consiste à penser que la bonne interface est celle qui réussit à transposer métaphoriquement le monde humain en interface-utilisateur, comme l’illustre le modèle Graphic User Interface (GUI) conçue dans les années 1970 par le Palo Alto Reseach Center (PARC), à la base du bureau (desktop). Or d’une part, le « paradigme de la métaphore », compris comme l’intuition (on intuiting) des choses, n’est qu’un paradigme parmi d’autres, à côté de celui du « paradigme technologique », basé sur la compréhension (on understanding) du fonctionnement des choses, ainsi que du « paradigme idiomatique », basé quant à lui sur le fait d’apprendre (on learning) comment accomplir les choses. D’autre part, s’il est certes aisé de concevoir des métaphores visuelles pour les objets physiques (poubelle, imprimante, document…) ou les activités courantes dans un environnement de travail (couper, copier, coller…), cela reste difficile voire impossible pour « des processus, des rapports, des services ou des transformations, fonctions pourtant fréquentes des logiciels » (Cooper 1995). Enfin, si les métaphores sont associées à des objets de l’âge mécanique (mechanical age artifacts), alors non seulement elles sont rudimentaires dans la mesure où elles n’exploitent pas le vrai pouvoir de l’ordinateur (the real power of the computer), mais en outre, elles nous contraignent aux usages hérités de ces mêmes objets."


Sullivan et Wright ne parlent pas d'une architecture fonctionnaliste. Ils ne décrivent pas des formes fonctionnelles. Derrière "forme", Sullivan évoque les sentiments, un au-delà fonctionnel. Ce choix du mot "forme" les rapprochent du vocabulaire de Darwin. On retrouve ici l'idée d'une pression, de contraintes environnementales impliquant des conséquences formelles. De ce point de vue, "forme" n'est pas seulement "visuel" ou "aspect" mais "organisation", "configuration", "conception". On peut retrouver la différence d'approche entre l'histoire naturelle et la biologie : entre le relevé de caractères sur le visible et l'étude de l'organisation interne.