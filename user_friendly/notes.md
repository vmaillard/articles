


Ted Nelson - Dream machines 

P. 12 : naive user

"I like to believe that both of these, in-
deed, are ten-minute ayatews— that i s , when w« get
them running, the range of uses shown here can be taught
to naive users in ten minutes or less"

"THE TEN- MINUTE RULE. Any system
which cannot be well taught to a layman in
ten minutes. by a tutor in the presence of a
responding setup, is too complicated. This may
sound far too stringent; I think not. Rich and
powerful systems may be given front ends which
arc nonetheless ridiculously clear: this is a
designp roblem of the foremost importance"


---

Jeff Johnson

"this design approach is intended to facilitate one’s use of the system by making the manipulation of information in the system analogous to the manipulation of physical objects on a desktop. The choice of office objects in particular is intended to facilitate learning by capitalizing on users’ familiarity with such objects and with procedures involving them"

Smith, D. C., Ludolph, F. E. & Irby, C. H. (1985). "The Desktop Metaphor As an Approach to User Interface Design (Panel Discussion)", In Proceedings of the 1985 ACM Annual Conference on The Range of Computing : Mid-80's Perspective: Mid-80's Perspective. New York, NY, USA, pp.548-549. ACM

(Thèse Nolwenn Maudet)



Loretta Staples

http://lorettastaples.com/writing/blur.html

"This is a space where the medium threatens the message through its own palpable, manipulable presence, and where "user-friendliness" overrides authority as "readers" are given free reign to hop, skip, and jump through this phantom place. "

(Kevin Donnot, Technique & design graphique)

---

dictionnaire, dictionnary of computing

https://foldoc.org/user-friendly

http://www.catb.org/jargon/html/U/user-friendly.html

---

Moshé M. Zloof :

https://dblp.uni-trier.de/pid/68/3899.html


Engelebart : https://dblp.uni-trier.de/pid/e/DouglasCEngelbart.html

---

GUI

https://www.mprove.de/visionreality/media/kay72.html

Licklider - Man-Computer Symbiosis

Robert (Bob) Taylor

https://www.nytimes.com/2017/04/14/technology/robert-taylor-innovator-who-shaped-modern-computing-dies-at-85.html

The Computer as a Communication Device, J.C.R. Licklider and Robert W. Taylor

---

User friendly

Tim Bosenick, Mensch & Computer 2005 A Hybrid Approach for Qualitative and Quantitative Usability Studies, 2005

```
"Qualitative usability tests belong to the standard repertoire of companies who want to design user-friendly websites or to develop computer applications best suited to the users (cf. Nielsen 1993, Mayhew 1999, Travis 2003)"
```

* Nielsen, J. (1993): Usability Engineering. San Francisco, CA: Morgan Kaufman
* Mayhew, D. J. (1999): The Usability Engineering Lifecycle. San Francisco, CA: Morgan Kaufman
* Travis, D. (2003): E-Commerce Usability. Tools and Techniques to Perfect the On-line Experience. Taylor & Francis Group

M. D. Apperley, "User-oriented interactive graphics," in DECKS Europe Conference Proceedings, Zurich, Switzerland, 1974

https://www.cs.umd.edu/users/ben/papers/Shneiderman1982Teaching.pdf

Shneiderman, Software psychology, 1980

```
p.2-3
"The intuition and experience of programming language and application system designers can and should be supplemented with experimental research to aid in producing high-quality 'user-friendly' interfaces, especially for nonprogrammers."

p.174
"Zloof (1978) offers nine requirements for design of a user-friendly data manipulation language for nonprogrammers:

1) Minimum concepts required to get started: simple operations should be simple.
2) Minimum syntax: simple syntax, even for complex operations.
3) Consistency: operators should have consistent semantics in all contexts.
4) Flexibility: language should 'capture the user's thought process, thus providing many degrees of freedom in formulating a transaction.'"

McDonald, N. and M. Stonebraker, CUPID: The friendly query language, Proceedings of the A CM Pacific Conjerence, San Francisco, (April 17-18, 1975).
```

L. R. Harris, User oriented data base query with the ROBOT natural language query system, 1977

```
"There is considerable effort required to make any computer system "user friendly"."
```

https://www.sciencedirect.com/science/article/abs/pii/S0020737380800162

Zloof, M.M., Design aspects of the Query-by-Example data base language, In B. Shneiderman (Ed.), Databases.. Improving Usability and Responsiveness, Academic Press, New York, (1978), 29-54.

```
p. 30

p.36-37
"It is only recently, when the use of computers has infiltrated the non-programmer community, that the words "ease of use", "user friendly" and "human factors" were added to the lexicon of computer sciences terminology. The reasons these considerations did not receive earlier attention are twofold: first the use of computers was limited to the professional programmers; and second, since computers were relatively expensive, slow, and limited in memory and other storage devices, language designers were (maybe justifiably) mainly concerned with performance and implementation considerations. Today, on the other hand, the ease of use aspect of the language is receiving attention, because a non-programmer user must find it easy to learn and use, or it will be neglected, no matter how fast, efficient, and powerful the system is.
The question now arises: what is ease-of-use and how can one define and quantify it? Ease-of-use and user-friendly are fuzzy variables--far from being rigorously defined. While it is relatively easy, for example, to measure and compare the speed of two machines, it is quite difficult to define ease-of-use, or say that one language is twice as user-friendly as another; the terms are subjective and depend on the user's taste, background, environment, training, etc. Recently (7,22,23) researchers have tried to measure different languages in terms of average time required to formulate a set of queries, percentage of correct queries, confidence ratings, classification of errors, etc.
Since Query-By-Example was found to be highly user-friendly (7), and because it is being accepted by the non-programmer community, we shall try to give our considerations when designing a language."
```

* constats :
  * les ordinateurs sont de moins en moins chers
  * les ordinateurs sont de plus en plus répandus dans des communautés de non-programmeurs
  * il y a un besoin d'automatisation des traitements de bases de données
  * l'intérêt pour les questions de performance, de rapidité, d'implémentation se déplace vers la facilité d'utilisation
* comment construire des langages de requêtes haut niveau faciles d'utilisation ?
  * construction avec des retours d'utilisateurs non-programmeurs
  * réalisation d'études psychologiques
  * on mesure le caractère "user-friendly" par la capacité à apprendre le langage rapidement pour pouvoir alors réaliser des requêtes complexes (3h pour Query-By-Example)
  * Codd : permettre l'utilisation de la langue maternelle / langages naturels
  * Zloof : pas pour les langages naturels mais pour les langages formels de haut niveau comme intermédiaires
* typologie d'utilisateurs :
  * l'utilisateur occasionnel (pas de connaissances en programmation)
  * le professionnel non-programmeur (familier des concepts de programmation, peut apprende un langage formel simple)
  * le programmeur (connaissances de plusieurs langages de programmation)
* conditions pour déterminer le niveau "user-friendly" d'un langage :
  * peu de concepts à apprendre pour débuter
  * syntaxe simple
  * homogénéité de la syntaxe suivant les contextes
  * flexibilié dans la formulation
  * pas de grands changements lors de l'ajout d'une nouvelle condition
  * ...

J.P. Fry and T.J. Teorey, Simplifying design and implementation, In B. Shneiderman (Ed.), Databases.. Improving Usability and Responsiveness, Academic Press, New York, (1978)

```
p.180-181
```

Nancy McDonald and Michael Stonebraker, *CUPID- The Friendly Query Language*, EECS Department, University of California, Berkeley, 1974-1975

* 

https://www2.eecs.berkeley.edu/Pubs/TechRpts/1974/ERL-m-487.pdf

p. 8 "The advantages of this syntax over a natural language system are now summarized: 5. User-friendly with its menu-move operations."

J . Beetz, APLSV for a flexible man-computer dialog, 11 June 1975,

https://dl.acm.org/doi/10.1145/800117.803782

```
p. 37
"Despite a lot of talk about “user-friendly” systems, little is seen when it comes down to the implementation, mainly because most programming languages and time-sharing systems make it relatively difficult to give life to the theoretical concepts."
```

Seven steps to RENDEZVOUS with the casual user, E.F. Codd, January 17, 1974

* 

https://stacks.stanford.edu/file/cp353fq9623/cp353fq9623.pdf \*

https://purl.stanford.edu/cp353fq9623

T. A. Dolotta, Data Processing in 1980-1985: A Study of Potential Limitations to Progress, 1976

https://archive.org/details/dataprocessingin00dolo/page/154/mode/2up?q=user

```
p. 104-105
"All of the above aspects of data processing systems can be combined to cover the attrinbute that we call "user-perceived quality." This term focuses particular attention on concepts derived from the field called ergonomics or human engineering. Concepts such as ease of use, minimization of inadvertent errors, "fail soft," recovery, forgiveness, etc., must become an integral part of the conception and design of ail software. Included is an understanding by designers and developers of the end users' perception of the way things should be done and of what impact or considérations must be attributed to these perceptions in a proper trade-off study."

p. 154-155
usability
```

Lucas, H.C. Jr. A user oriented approach to systems design, Proc. ACM 1971 Annual Conf., pp. 325-338.

https://sci-hub.st/https://doi.org/10.1145/800184.810503

G. M. Weinberg, "The Psychology of Computer Programming", Van Nostrand Reinhold Co., New York, 1971.

1983 :

https://books.google.fr/books?id=200qAQAAMAAJ&q=user-friendly&dq=user-friendly&hl=fr&sa=X&ved=2ahUKEwiKyKiBvPPtAhUK3xoKHaIlDSMQ6AEwBXoECAIQAg ngram :

* 

https://books.google.com/ngrams/graph?content=user-friendly&year_start=1800&year_end=2019&corpus=26&smoothing=0# \*

https://www.google.com/search?q=user-friendly&biw=1916&bih=926&sxsrf=ALeKk02Jk1yHuASNRvQ5Czd1JSYRsEdqCA%3A1609254407260&source=lnt&tbs=cdr%3A1%2Ccd_min%3A1%2F1%2F1933%2Ccd_max%3A12%2F31%2F1936&tbm=bks

http://gordonbrander.com/pattern/bootstrapping/

termes :

* straightforward
* ease of use
* easy to use
* user friendly
* usability
* user-oriented
* fail soft
* user-perceived quality

Inverse :

* difficult
* clumsy



Wendy Hui Kyong Chun dans Programmed visions, nous aide pas trop :
"In the 1990s (and even today), textbooks of human – computer interface (HCI) design described metaphors as central to “user-friendly” interfaces."

Par contre, Thierry Bardini dans Bootstrapping: Douglas Engelbart, Coevolution, and the Origins of Personal Computing est hyper intéressant. Il a pas mal de sources dont notamment des interviews persos avec des gens à Xerox (dont Engelbart). Et il aborde qq chose que je ne savais pas : Engelbart était opposé à la vision de Xerox dans la poursuite de leur travail et puis, il critique pas mal "user-friendly".

C'est Bardini qui écrit :

p. 54
"Learning is not necessarily easy, especially at the somatic level. It is important not to confuse the way Engelbart conceived the prosthesis of the user-machine interface with later concepts such as “user friendliness.” Quite the contrary. As Engelbart later put it, “Concern with the ‘easy-to-learn’ aspect of user-oriented applications systems has often been wrongly emphasized. For control of functions that are done very frequently, payoff in higher efficiency warrants the extra-training costs associated with using a sophisticated command vocabulary, including highly abbreviated (therefore non-mnemonic) command terms, and requiring mastery of challenging operating skills” (Engelbart 1973, 223)."

p.154
Thus, the “bootstrapping philosophy” proposed by Douglas Engelbart at ARC during the 1960’ ended up being put to use for very different ends than those envisioned by his crusade. 6
To Engelbart, this appropriation of the bootstrapping methodology was a betrayal, and it masked a rejection of the main principle of the implementation of the augmentation framework: “I got a very very strong personal impression that the Xerox clan just totally rejected all that stuff. They take parts of the architecture, they take the mouse, they take the idea of the windows, all of that, but the rest of NLS they just rejected” (Engelbart 1992).
What they rejected was the coevolution of user and machine and the concomitant requirement that the user undergo the rigors of a learning process. Although Engelbart claimed that NLS was not designed exclusively for computer professionals, as we have seen, his first model of the user, the intelligence worker, found its expression in the staff of computer programmers that he assembled in the laboratory and in the community that developed there. The bootstrapping philosophy of design put a high emphasis on learning, and the system was designed and implemented accordingly. It made demands on its users, and Engelbart made no effort to make it “user-friendly.”

6. For this section on Douglas Engelbart’s contribution, I again rely on my own interview as well on four interviews carried out by Henry Lowood, bibliographer for History of Science and Technology Collections at Stanford University, in 1987-88 (Engelbart 1996). I again thank Henry Lowood for giving me access to these materials.

Bardini nuance ensuite en disant que d'autres ont bien continuer dans la veine que voulait Xerox.

p.160
Dans une interview, W. K. English lui dit : "When we moved to PARC, we were at least thinking about the naive user. Xerox was a commercial company, and we were thinking we’d better build these systems so as the average person can use the technology... I think the only model we had, again, were the people around us. Secretaries. We would look at the nontechnical people as the users”"

---

Et puis il y a un certain Benjamin Shneiderman qui revient pas mal. Dans son article "The future of interactive systems and the emergence of direct manipulation" de 1982, il écrit :

"This author believes that the route to rapid progress is through controlled psychologically-oriented experimentation (Shneiderman 1980) by academic  and industrial researchers and through pilot and acceptance tests by commercial  system developments. This disciplined approach complements and supports the intuition and experience of designers while replacing the vehement, but often empty, arguments over the 'user friendliness' of competing systems. Scientific measurement of learning time, performance speed, error rates, user satisfaction and retention provides valuable data for making design  decisions, choosing among competing products and formulating integrative theories which can be  used  for prediction in novel circumstances."

Pour l'instant, je crois que c'est la plus ancienne occurrence que j'ai pu lire. À lire, il a l'air de faire référence à un concept bien débattu. C'est écarté de la période Xerox Alto mais en tout cas ce ne sera sûrement pas dans des écrits de Engelbart !


