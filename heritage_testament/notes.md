

Bicentenaire de la mort de l’Empereur Napoléon Ier | Discours du Président Emmanuel Macron :

https://www.youtube.com/watch?v=zzV2t7s8LnU

Paraphrase de René Char à 34'10 : "Une nation palimpseste qui reçoit les héritages sans testament."

https://www.franceculture.fr/emissions/lesprit-public/le-mythe-napoleon-ou-la-nostalgie-dune-france-puissante

Saxifrage

* Lyon 04 2017 "j'ai aimé farouchement mes semblables ce jour-là"

https://livre.fnac.com/a10801190/Chantal-Delsol-Heriter-et-apres#int=:NonApplicable|NonApplicable|NonApplicable|10801190|NonApplicable|NonApplicable

https://www.cairn.info/formes-de-l-action-poetique--9782705693626-page-5.htm

https://books.openedition.org/res/1911?lang=fr#ftn45

https://umotion.univ-lemans.fr/forum-le-monde-le-mans/2016-heriter-et-apres/?page=1

https://www.lemans-tourisme.com/media/partenaire/fichiers/FMAPDL072V505RWV/f/o/Forum2016.pdf

http://forumlemondelemans.univ-lemans.fr/fr/archives/eme-forum-le-monde-le-mans-2016-nov-nouvelle-page.html?search-keywords=h%C3%A9riter

https://hal.archives-ouvertes.fr/hal-00684010/file/heriter_allege.pdf

https://www.placedeslibraires.fr/livre/9782267021844

---

Paul Veyne, René Char en ses poémes :

testament :

- 186
- 208
- 295
- 490

héritage :

- 385
- 425


--- 

Gracq - Préférence