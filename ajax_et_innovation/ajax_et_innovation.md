% Ajax et innovation
% Vincent Maillard
% 2019

≈ 8 000 signes

En ce nouveau samedi j’avais réussi à repousser d’aller voir jusqu’à après 18h. La première image à s’animer est celle d’un policier au geste sec qui de son poing ajuste un corps. Et me revoilà dans ma fin d’après-midi à alterner entre les chiffres arabes ou latins du hashtag d’un nouvel acte jaune. Les vidéos s’animent à mon passage au scroll, les messages se surajoutent.\
Cette satanée requête Ajax.\
Elle vient me notifier que de nouveaux tweets ont été ajoutés au hashtag depuis que je suis sur la page. Et elle est encore en train de tourner en douce. Plus l’activité est importante, plus elle renvoie les messages précédant au loin. Elle me détruit la continuité de la page. Son résultat est un fil d’ajouts successifs qui se superposent en se rejetant.

Ajax est un concept de programmation sur le web. Son principe repose sur l’utilisation du langage JavaScript pour, dans un premier temps, envoyer une requête au serveur et ensuite traiter la réponse reçue. Ainsi, il est possible de charger de nouveaux éléments dans une page sans pour autant la recharger.\
De nombreuses fonctionnalités telles que les messageries web, les cartes, les réseaux sociaux, l’auto-complétion lui doivent beaucoup car Ajax rend possible le caractère social et instantané du Web que nous connaissons. Ce n’est pas un fait récent car dès 2005, Aaron Swartz pouvait écrire\ : «\ New technology quickly becomes so pervasive that it’s sometimes hard to remember what things were like before it.\ » [^1]. Aujourd’hui ces pratiques se sont intensifiées, voire on même changer de nature avec l’usage du JavaScript côté serveur.

Le concept d’Ajax est utilisé à outrance pour capter l’attention de l’utilisateur des interfaces. Et ce caractère instantané — l’ajout de nouveaux messages, la mention que l’interlocuteur est en train d’écrire, la notification qui vient juste d’arriver — me paraît mensonger. En effet, le concept d’Ajax n’est pas instantané\ : entre la fonction qui fait la requête et celle qui reçoit la réponse, il existe un temps de latence inconnu. D’autre part, une requête ne bloque pas l’exécution d’autres scripts. Ajax s’exécute dans un temps particulier. Ce fonctionnement est dit «\ asynchrone\ ». (À l’inverse, un script est dit «\ synchrone\ » quand le passage d’une instruction à l’autre dépend de la bonne exécution de chacune.) La réduction du délai de réponse à quelques milli-secondes produit l’illusion de l’instantanéité.

Cette volonté d’ajouter du nouveau m’évoque l’idée d’innovation. J’aimerais ainsi me livrer à une analogie entre Ajax et l’innovation.

À la manière d’Ajax, une innovation est une prise de risque (Schumpeter) qui pour ajouter du nouveau dépend de son aboutissement. La validité d’une réponse Ajax est à vérifier, l’innovation ne garantit pas un succès. Il existe un temps inconnu pour vérifier de la réception d’une innovation, le délai entre la requête et la réponse d’Ajax est inconnu à l’avance. Ajax permet d’ajouter de nouvelles données à une page mais peut aussi opérer des changements plus globaux. L’innovation entretient un rapport ambigu au changement\ : est-il global, minime ou inexistant\ ? Ajax a permis à Google, Twitter et Facebook d’asseoir leur position dominante, l’innovation est le fer de lance de la croissance. Ajax est une fabrique du consensus (mise en avant d’actualités, analyse de tendances suggestion d’autocomplétion, [^2]), l’innovation dépend d’une diffusion large et engendre des comportements moutonniers (imitation d’une idée qui fait ses preuves).\
Rapport avec la répétition incessante\ : ajout de nouveaux tweets\ : les plus récents, ceux du bas de la page. Innovation\ : avancer pour rester sur place.

Le développement qui suit tente de décrire les instruments de pouvoir que sont la requête Ajax et l’innovation. Ainsi que les idéologies qui se dévoilent lorsqu’on cherche à utiliser l’innovation.

Je reviens plus précisément sur l’idée d’innovation. L’usage premier du terme est politique. Au XX^e^ siècle, le sens évolue et prend la forme de l’innovation technique. [^3] L’innovation technique est profondément liée à une certaine conception de l’économie\ : des entrepreneurs, des consommateurs et des financeurs. Elle dépend de l’estimation financière de sa valeur et du succès de sa diffusion.\
L’innovation est un procédé irrationnel au sens entendu de rationnel comme «\ la capacité à définir les moyens les meilleurs pour des fins qui sont bien identifiés\ » [^4]. En effet, il n’y a ni projets ni objectifs associés à l’innovation. Sur le projet qualitatif lié aux finalités des innovations destinées à nous faire vivre plus longtemps et sans maladies, Nicolas Bouzou répond ce qu’il a longtemps pensé\ : «\ débrouillez-vous\ ». [^5]

Et pourtant l’innovation est courtisée quand il s’agit de relancer une économie, voir même de répondre à l’urgence climatique. L’innovation entretient un rapport flou sur son projet et son rapport au changement qu’elle promet. À ce titre, Benoit Godin revient sur la conception qu’en a Machiavel\ : «\ En définitive, l’innovation constitue, pour Machiavel, un moyen de stabiliser un monde turbulent – et non un moyen de le révolutionner, comme l’affirment aujourd’hui les théoriciens de l’innovation.\ »\
Ainsi\ : innovation n’est pas synonyme de révolution mais plutôt de conservation. Elle fait état du paradoxe de la croissance qui consiste à croître pour rester à sa place. [^6]\
D’un point de vue technique l’innovation induit alors l’obsolescence puisqu’elle remplace à l’infini. 

Lors du récent débat avec les intellectuels [^7], Philippe Aghion est revenu sur l’idée d’innovation et sur sa désillusion du «\ en même temps\ » macronien. Il souhaiterait utiliser la logique d’innovation pour répondre à la contrainte climatique.
Aghion dit\ : «\ Donc l’idée c’est de dire je vais utiliser les forces du marché, la concurrence, la réglementation, d’autres forces et on va être très innovant en matière de lutter contre le réchauffement climatique par delà une taxe carbone qui elle serait plus intelligente.\ »\
Une requête Ajax est asynchrone, une innovation également. Ce «\ en même temps\ » l’est-il\ ?\
Ainsi, après avoir exploité sans vergogne les ressources naturelles, voici qu’il faudrait, pour changer, adopter les conditions de la concurrence pour exploiter ces ressources mais cette fois de façon durable. C’est curieux car comme le rappelle Hartmut Rosa dans Aliénation et accélération, les principes de la compétition d’une économie capitaliste résident dans la réduction du temps de production (et donc réduction des coûts) ainsi que la recherche d’un retour sur investissement et donc une accélération de la circulation et de la consommation.\
Ce qui nous a amené dans la situation actuelle.\
Alors que la disruption c’est une stratégie pour devancer ses concurrents mais aussi ses régulateurs. [^8]\
Aghion est déçu par le «\ en même temps\ » qui aurait été «\ retoqué\ ». Mais aurait-il oublier le caractère asynchrone de l’innovation\ ? Le «\ en même temps\ » du candidat puis président Macron est ambigu\ : est-il synchrone ou asynchrone\ ? Les innovations ne réfléchissent pas au même temps, elles cherchent à se dépasser. Elles sont asynchrones et non synchrones. Leur temps de réponse est inconnue, il n’y a pas à chercher d’y ajouter des événements en parallèle. Elles poursuivent leur propre exécution. Le «\ en même temps\ » existe dans l'esprit de ce que retourne la promesse et surtout dans son délai d'exécution.

L’innovation porte en elle un temps anhistorique.\
La requête Ajax de Twitter n’est pas un outil de sauvegarde ou de pensée historique. Revenir sur les hashtags des anciens actes jaunes, c’est plonger dans une masse informe où se sont agrégés d’autres mouvements (les manifestation en Algérie), les fautes de frappe, les tweets écrits plus tard, avant, etc…\
Il y a cette tension étrange entre la masse de ces images, leur importance dans le temps de leur diffusion et ce flux mouvant qui ne me permet pas vraiment d’archiver. J’imagine des personnes en train de télécharger le contenu de peur qu’il ne disparaisse, ne soit supprimer par la plateforme. La fuite des tweets participe du complotisme. Si l’on ne retrouve pas ce qu’on avait vu c’est que quelqu’un l’a décidé\ ? Quelle est la vision de l’histoire de ces plateformes\ ? Pourquoi ces plateformes ne sont-elles pas des outils permettant de retrouver convenablement ce qu'on avait lu il y a quelques jours\ ? 

[^1]: Aaron Swartz, «\ A brief History of Ajax\ », URL\ : [aaronsw.com/weblog/ajaxhistory](http://www.aaronsw.com/weblog/ajaxhistory).

[^2]: «\ Les pouvoirs éditoriaux de Google\ », *Communication & langages*, NecPlus, n^o^188, 2016.

[^3]: Benoît Godin, *L’innovation sous tension : Histoire d’un concept*, Presses Universitaires de Laval, 2017.

[^4]: Norbert Alter, «\ L’innovation, moteur du capitalisme\ ?\ », France Culture.

[^5]: France Culture - Grande Table, 05/12/2016 et Nicolas Bouzou, «\ L’innovation sauvera le monde\ ».

[^6]: cf. Pente glissante - Hartmut Rosa dans Hartmut Rosa, *Accélération : Une critique sociale du temps*, Éditions La Découverte, 2013.

[^7]: [https://www.franceculture.fr/emissions/grand-debat-national-echange-avec-des-intellectuels-1ere-partie
](https://www.franceculture.fr/emissions/grand-debat-national-echange-avec-des-intellectuels-1ere-partie
)

[^8]: Bernard Stiegler, «\ La disruption rend fou\ », URL\ : [https://vimeo.com/183850750](https://vimeo.com/183850750).
