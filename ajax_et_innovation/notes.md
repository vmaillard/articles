Cette innovation qui tend à reléguer et à invalider ce qu’elle voudrait changer.

Assoie la puissance
Asynchrone, en attente de la réponse
Simulation de l’hisorique
Consensus du temps réel : résultat de recherche en live / résultat populaire / autocomplétion

donc une accélération de la circulation et de la consommation.

---

Fils dʼactualité
actualité -> actualisation -> réactualisation 
Moteurs de recherche
vote (page ranking)  -> popularité -> recommandation -> réussite

---

D. Cardon : futur des algo prédit par le passé des gens qui vous ressemblent

---

Assemblée nationale et les votes de nuit :
15 septembre 2018 à 4h du matin : Contre la mention dans la loi de l’interdiction du glyphosate d’ici 2021 : https://www.huffingtonpost.fr/2018/09/14/lassemblee-nationale-refuse-encore-dinscrire-linterdiction-du-glyphosate-dans-la-loi_a_23527784/
19 mars 2019 à 6h du matin : Pour la privatisation des aéroports de Paris : https://twitter.com/JLMelenchon/status/1107980242793189377

---

Grand débat national : échange avec des intellectuels
Prise de parole de Philippe Aghion 

https://www.franceculture.fr/emissions/grand-debat-national-echange-avec-des-intellectuels-1ere-partie
à 36'15
à vérifier : dit-il "innovation verte" ou "innovation ouverte" ?

"Je crois qu’effectivement pour assurer la progression du pouvoir d’achat, malgré une double contrainte démographique et climatique, il n’y a pas d’autres moyens que d’avoir une économie d’innovation.
Donc il faut créer les moyens d’une économie qui incite à l’innovation et le faire d’une manière qui soit inclusive et qui pousse à l’innovation verte.
Je crois qu’une des choses qu’on a appris avec le mouvement c’était que l’approche au vert a été perçue comme un peu punitive avec la façon dont la taxe carbone avait été mise en place.
Et donc ça pousse à se dire il faut la taxe carbone, il faut peut-être la mettre en place avec des modalités différentes. En tenant compte des fluctuations des prix du carburant.
Donc là il y a eu des idées qui ont été émises. Et puis je crois que de dire que des recherches ont été développées qui sont alternatives, qui sont complémentaires de la taxe carbone. 
Par exemple, le rôle de l’investissement, de l’aide à l’innovation verte évidemment.
Et puis ce qu’on appelle la Corporate Social Responsability. C’est-à-dire que maintenant, il y a vraiment cette idée qu’on peut pousser les entreprises à devenir plus responsables.
Et là il y a eu des idées intéressantes qui ont été émises. L’une c’est l’idée que la concurrence peut aider. Combiner la concurrence et le fait que les citoyens sont conscients du danger climatique. La combinaison des deux pousse les entreprises à l’innovation verte.
Donc ça c’est un mélange de politique de concurrence et de politique d’éducation.
Mais il y a eu aussi des idées très intéressantes par un ami à moi, Patrick Bolton et d’autres, sur l’idée de Climate Stress Test. C’est l’idée de dire : on propose un scénario climat adverse par exemple : un réchauffement climatique plus rapide que prévu, une montée des eaux plus rapide que prévue et on regarde pour chaque entreprise, qui est côtée, est-ce qu’elle fait face ou pas à ce risque ? 
Et on peut réglementer ensuite les entreprises comme ça. C’est-à-dire : vous faites un certain nombre d’investissement : comment vous répondez à cette Stress Test ?
Donc l’idée c’est de dire je vais utiliser les forces du marché, la concurrence, la réglementation, d’autres forces et on va être très innovant en matière de lutter contre le réchauffement climatique par delà une taxe carbone qui elle serait plus intelligente.
Donc je crois que là, c’est une chose que l’on avait pas suffisamment réfléchie je crois au moment de la campagne : comment pousser au vert ? Comment pousser à l’innovation verte ? Avec toute une série d’instruments qu’on avait pas pensé avant.

Je voulais quand même dire un dernier mot sur les niches fiscales.
Quand on était plusieurs impliqués dans le programme et moi en tout cas, j’avais mal compris. J’étais beaucoup pousser pour la flat tax parce que je pensais également qu’il fallait devenir un pays comme les autres en matière d’investissement : pousser à l’innovation, pousser à l’investissement. Il fallait qu’on montre qu’on était comme la Suède. Moi mon modèle, c’était la Scandinavie : ils ont la flat tax, pourquoi on ne l’aurait pas ?
Mais il y a avait pour moi le « en même temps ». Et le « en même temps » c’était les niches fiscales. C’est-à-dire qu’en Suède, il y a beaucoup moins de niches fiscales que chez nous par exemple. Nous on a à peu près 450 niches pour un total de 100 milliards d’euros. Il y a une grande complexité du système des niches, une grande incohérence. Et dans mon esprit, mais c’était aussi dans l’esprit de gens, qui venaient par exemple de Rexecode ou d’autres qui ne sont pas particulièrement étiquetés à gauche, qui poussaient également pour la flat tax. Et puis pour eux, il y avait le « en même temps » des niches fiscales. Et il y a une sérénite/sévérité auquel on peut penser : on a des plafonnements à 10 000 euros pour les niches individuelles, on peut les baisser. On peut élargir l’assiette de la niche Copée. Il y a des abattements de 10% pour frais professionnels dont bénéficient beaucoup de hauts salaires, des hauts retraités, etc, on peut peut-être toucher à ça. J’en sais rien. 
Mais quand j’ai vu le ministre de budget, venir avec l’idée : oui on va s’attaquer aux niches fiscales, j’étais plutôt content parce que j’ai dit c’est une bonne chose. Ce « en même temps » qu’on aurait peut-être dû faire avant tout d’un coup, il vient. Mais après, j’ai eu le sentiment que c’était un peu retoqué, que on était pas sûr qu’on allait le faire.
Et dans mon esprit, il y avait l’idée vraiment de ce « en même temps » : on va vers une fiscalité plus simple mais également et bien les niches fiscales on ajuste, on les met à plat. Ça veut pas dire qu’on les supprime toutes mais ça donne un signal si vous voulez d’équilibre. Il y a le fameux « en même temps » et je crois qu’un petit peu la perception était de dire : on ne sent plus le  « en même temps » et ça part trop d’un côté."

---

John Maeda :
« Et en collaboration avec Google Design, je vais présenter les éléments clés du Material, un langage de conception utilisé par de nombreuses entreprises gratuit et accessible à tous. En l’utilisant, vous avez accès à des techniques de conception d’utilisateurs expérimentés. C’est une manière de partager ce savoir avec le reste du monde. Avec le Design In Tech Report vous pourrez jouer avec les ombres, la couleur. Vous apprendrez aussi comment le design classique interagit avec le design computationnel. C’est ce que vous découvrirez. »
https://www.biennale-design.com/saint-etienne/2019/fr/programmation/?event=design-in-tech-1

https://design.co/dit2019-v007/#29
https://designintech.report/2019/03/09/design-in-tech-report-2019/

---

Ajax un héros qui vient de loin
https://www.academia.edu/38889455/Ajax._Un_h%C3%A9ros_qui_vient_de_loin

---

"Mark Zuckerberg : 'Quatre idées pour réguler Internet'", Le Journal du dimanche, 30 mars 2019

https://www.lejdd.fr/Medias/exclusif-mark-zuckerberg-quatre-idees-pour-reguler-internet-3883274


---

Hard Questions: Is Spending Time on Social Media Bad for Us?, 15 décembre 2017
https://newsroom.fb.com/news/2017/12/hard-questions-is-spending-time-on-social-media-bad-for-us/

"In sum, our research and other academic literature suggests that it’s about how you use social media that matters when it comes to your well-being."
Oui et Bernardo Gui, l'inquisiteur des interfaces graphiques qui vous torture pour vous faire avouer ce que vous n'avez pas fait ?

---

https://www.franceculture.fr/emissions/matieres-a-penser/face-a-la-catastrophe-le-pire-nest-jamais-sur-44-place-aux-initiatives-citoyennes
« Le système qui est en train de détruire notre environnement est le même que celui qui est en train de détruire notre rapport à l'information. » (Anne-Sophie Novel)

--- 

Si l’on peut considérer les êtres vivants comme des vestiges d’un temps où la Terre réunissait les conditions nécessaires à l’apparition de la vie, tentons de penser le design comme un vestige de la Révolution Industrielle. Consultons le commandant Van der Weyden, qui après s’être demandé devant un tas de brun tombé du ciel si « c’est du vivant ? », pour voir si devant nos interfaces de réseaux sociaux il se demanderait si « c’est du design ? ». Et d’aller ensuite chercher « la scientifique » dans l’espoir d’une authentification.
