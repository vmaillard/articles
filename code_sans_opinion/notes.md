Code sans opinion/agnostique :

https://processwire.com/talk/topic/23184-bootstrap-vs-any-other-css-framework/

Sur le code sans opinion :

https://gist.github.com/maximelebreton/fdd484643f96a328006fea8e0f315996

où on trouve : "Because I think that the best css/js framework is composed by many agnostic micro libraries."

Pour Tailwind, leur page d'accueil n'a plus la mention sur l'opinion. Mais dans les archives, on trouve en 2017 :

"On the flip side, it also has no opinion about how your site should look and doesn't impose design decisions that you have to fight to undo."

en 2019 :

"Tailwind CSS is a highly customizable, low-level CSS framework that gives you all of the building blocks you need to build bespoke designs without any annoying opinionated styles you have to fight to override"

et :

"Instead of opinionated predesigned components, Tailwind provides low-level utility classes that let you build completely custom designs without ever leaving your HTML."

Sur l'agnosticisme, un filtre pandoc sur page github (https://github.com/odkr/pandoc-quotes.lua/blob/master/man/pandoc-quotes.lua.md) :
"pandoc-quotes.lua is Unicode-agnostic."

https://github.com/Netflix/pollyjs (2022.01.23) : "Polly.JS is a standalone, framework-agnostic JavaScript library that enables recording, replaying, and stubbing of HTTP interactions."

Code avec opinion :


Symfony 5: The Fast Track Fabien Potencier (2022-01-24) https://symfony.com/doc/current/the-fast-track/en/1-tools.html#opinionated-choices :

"I want to move fast with the best options out there. I made opinionated choices for this book."

Weasyprint : https://doc.courtbouillon.org/weasyprint/stable/going_further.html

"Python is a really good language to design a small, OS-agnostic parser."

https://processwire.com/talk/search/?q=unopinionated&quick=1

"I know there are some parallel topics going on here and this was perhaps related to another idea altogether, but: this is a very good point, in my opinion. "React made me do it", or "it's like this because components" are not very good excuses to mess things up. It's just tech, and particularly unopinionated tech for that matter. The problem isn't React or components – it's how people use them"

"Note, that while these libraries are similar, but they are far from being the "same". Unipoly is opinionated while HTMX "is not" (at least not that much...), which is a big difference."

Unpoly :

- https://unpoly.com/
- https://blog.teamtreehouse.com/unobtrusive-javascript-important

"The unobtrusive JavaScript framework for server-side web applications"

Mdash :


"Mdash is a small, framework-agnostic library of components that are comprised of standard HTML, custom HTML, and Custom Elements." https://getkirby.com/kosmos/66

"M- has 150+ utility classes and 20 components all built from real web standards and there are NO build steps, NO dependencies, it's framework-agnostic, and is somehow a tiny 6.8kb: m-docs.org" https://dev.to/jfbrennan/comment/1d0dj

https://m-docs.org/

Headwind

"Headwind is an opinionated Tailwind CSS class sorter for Visual Studio Code. It enforces consistent ordering of classes by parsing your code and reprinting class tags to follow a given order."

https://marketplace.visualstudio.com/items?itemName=heybourn.headwind