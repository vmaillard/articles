# Quand l'interface nous parle

Une interface graphique peut être définie comme un ensemble de signes
visuels permettant l'utilisation d'un objet numérique. Suivant cette
définition, l'interface pourrait apparaître comme un objet inerte.
Cependant, comme le souligne Guillaume Giroud (doctorant en
philosophie), « le sens de l'interface ni ne se présente, ni ne
transparaît, toujours altéré qu'il est par la médiation qu'elle opère.
Il ne s'appréhende donc pas immédiatement, mais seulement par ses
effets[^1] ». Il insiste alors sur la relation que l'interface établit
avec d'autres objets sous le mode d'un médiateur, et non d'un
intermédiaire neutre. Pour explorer les effets actifs des interfaces
graphiques, je vais me concentrer sur des interfaces mettant en scène
l'échange de messages. Mon analyse a pour départ des œuvres de fiction
mais l'enjeu est d'étudier progressivement les interfaces dans leur
usage quotidien. Le passage par des œuvres de fiction sert à se rendre
compte de certaines particularités narratives, voire fictionnelles, des
interfaces graphiques.

Les interfaces d'échange de messages représentent l'échange entre *a
minima* deux personnes, en dehors de contraintes spatiales ou
temporelles et par le biais de protocoles techniques. Elles représentent
l'instantanéité de l'échange et mettent en scène une neutralité dans le
transfert d'informations. Il existe ainsi une première adhésion :
l'interface se présente comme un intermédiaire fiable, n'invitant pas à
se questionner sur l'identité des personnes ou les solutions techniques.
Une seconde adhésion consiste à ce qu'un utilisateur prenne part à
l'échange de messages. Ainsi, ces interfaces sont des représentations
dans lesquelles l'utilisateur s'engage. Cette réthorique d'intermédiaire
neutre et fiable n'aide pas à s'interroger sur les effets des
interfaces. Elle évoque la « naturalisation » de l'usage des interfaces
dont parlent Loup Cellard et Anthony Masure[^2] pour décrire ce sur quoi
reposent nos interactions avec celles-ci : la dissimulation technique,
l'illusion d'immédiateté et de continuité avec le quotidien. Je cherche
à étudier ici cette naturalisation, non pas pour l'annuler mais pour
m'interroger sur les manières dont on peut être crédule face à des
interfaces. C'est envisager les interfaces comme créatrices de récit et
s'interroger sur les conditions de ces récits.

*naturalisation => habitude ?*

## Niveaux narratifs

Des échanges qu'elles représentent, les interfaces graphiques en
définissent les limites, les conditions et les règles d'affichage, tout
en restant ouvert à l'engagement d'utilisateurs pour de futurs messages.
Elles en sont autant les décors, les acteurs, les narrateurs que les
scripts. Ces multiples rôles sont particulièrement visibles dans le
court-métrage *Noah* (Patrick Cederberg, 2013). C'est ce qu'on appelle
un *desktop film* car il est entièrement « filmé » par la fonctionnalité
de capture d'écran. Il ne montre ainsi que ce qui est visible dans le
cadre d'un écran. Toutes les actions et les réactions des acteurs
prennent place et sont traduites par le biais d'interfaces.

Sous ce mode visuel, le film raconte la rupture amoureuse de Noah et
Amy, en même temps que Noah la raconte à un de ses amis ou bien la
diffuse sur Facebook en usurpant l'identité d'Amy. Par la multiplication
des fenêtres, la narration offre l'expérience simultanée de plusieurs
récits, elle fait ainsi de Noah à la fois un personnage d'un récit et le
narrateur d'un autre. De plus, ces récits sont perméables car des
événements de l'un ont des conséquences sur leurs récits voisins. Cette
construction narrative décrit ce qui pour Gérard Genette caractérise une
différence de niveaux narratifs, à savoir que « tout événement raconté
par un récit est à un niveau diégétique immédiatement supérieur à celui
où se situe l'acte narratif producteur de ce récit[^3] ». On peut dire
que la narration de *Noah* est construite sur plusieurs niveaux
narratifs. Lorsque Noah échange avec à son ami par le biais de la
messagerie Facebook, il rejoue en quelque sorte l'acte narratif dont
procède le film : raconter ce qu'il lui arrive dans une interface.

Le film repose donc sur un effet narratif des interfaces. Elles
médiatisent la narration et en cela permettent à Noah d'être lui-même
narrateur. De ce point de vue, l'apport des concepts de narratologie de
Gérard Genette, comme évoqué plus haut, me paraît pertinent. L'exemple
des niveaux narratifs dans *Noah* m'interroge sur la « frontière
mouvante[^4] » entre l'acte narratif et le récit, ce que Genette désigne
comme étant deux mondes de la narration : « celui *où* l'on raconte,
celui *que* l'on raconte[^5] ». Sur l'usage de la narratologie à propos
de récits littéraires interactifs, et donc en-dehors de son champ
d'action originel, Serge Bouchardon écrit : « les théories du récit
affichent, pour la plupart, une universalité indépendante du
support[^6] ». Je prends alors pour hypothèse de pouvoir utiliser *les
théories du récit* afin d'analyser les effets des interfaces. J'ai
d'ailleurs utilisé plus haut le principe d'adhésion « propre à tout
récit[^7] » pour le décrire comme un effet de celles-ci.

L'usage narratif des interfaces est également présent dans l'incipit du
film *Matrix* (Lana et Lilly Wachowski, 1999). Dans cette scène, le
personnage principal (Neo) est soudainement réveillé par l'affichage sur
son écran d'ordinateur de messages provenant d'un énigmatique émetteur.
Ces messages s'adressent à Neo, comme un acteur pourrait le faire, mais
en même temps décrivent ce qui va se passer dans le film, comme un
script. Ainsi, l'interface fusionne acteur et script : elle a la
capacité d'intervenir sur Neo en le réveillant, et d'énoncer la suite de
l'histoire sous le mode impératif. J'utilise ici le terme *script* dans
un double sens : celui du cinéma où le script permet de lire le scénario
et celui de l'informatique d'une série d'instructions à exécuter. Le
personnage est face à une interface qui dépasse son cadre : elle
s'adresse spécifiquement à lui et il lit un récit qui raconte sa propre
histoire, elle le fictionnalise. L'interface est le seuil d'un espace
narratif dans lequel se poursuit le film.

Dans ces deux films, l'usage d'interfaces d'échange de messages a pour
effet de multiplier les niveaux narratifs. Dans *Noah*, c'est le
personnage principal qui prend en charge ces passages car « la
navigation construit la narrativité[^8] », comme le déclare Anaïs Guilet
(maîtresse de conférence en littératures comparées) à propos des
*desktop films*. Dans *Matrix*, le mouvement est différent : c'est
l'interface qui produit d'elle-même une narration avec des effets sur
l'histoire. C'est d'ailleurs un des moteurs du film que de déterminer
qui raconte l'histoire. Il y a ici un trouble sur la nature de Neo et
des messages qu'il reçoit. D'un point de vue narratif, il y a une
transgression de niveaux : l'interface produit un récit en avance sur
celui du film.

Cette transgression de niveaux correspond à la figure de la métalepse,
c'est-à-dire un franchissement de niveaux narratifs qui brouille la
relation entre l'histoire racontée et l'acte narratif la produisant.
Gérard Genette la décrit comme « une manipulation » de la relation du
« producteur d'une représentation à cette représentation
elle-même[^9]. » Il donne, à titre d'exemple, la nouvelle « Continuidad
de los Parques » de Julio Cortázar où un personnage est assassiné par un
personnage du roman qu'il est en train de lire. La figure de la
métalepse me semble être bien présente dans *Matrix* où le récit de
l'interface vient faire irruption dans celui du film. Le personnage s'y
conforme malgré son incrédulité, le spectateur s'interroge. Ce dernier
se doit d'être attentif pour repérer les moments où un niveau narratif
est remis en question. L'adhésion au récit, le « contrat
fictionnel[^10] », est malmenée : on pense suivre l'histoire de Neo
alors que celui-ci est fictionnalisé par un autre récit. Ce trouble
porte atteinte à « la relation entre un niveau (prétendu) réel et un
niveau (assuré comme) fictionnel[^11] », si bien qu'on ne sait plus
déterminer ce qui relève de la fiction, ce qui peut en échapper ou ce
qui est producteur du récit. La crédulité du personnage et du spectateur
est stimulée par les transgressions narratives permises par l'interface.

Dans son analyse des récits littéraires interactifs, Serge Bouchardon
parle de « métalepse dispositive » pour décrire « une forme inédite de
métalepse dans la mesure où le franchissement de seuil ne concerne plus
un niveau diégétique mais le dispositif lectoriel lui-même[^12]. » Ces
récits interpellent le lecteur en franchisant leur environnement
technique de lecture. Il y aurait ainsi une pertinence à utiliser la
métalepse pour décrire les effets des interfaces, du moins lorsqu'elles
sont utilisées dans un contexte fictionnel.

## Interactivité

L'adhésion à la représentation d'un échange de messages par une
interface passe également par l'engagement de l'utilisateur :
l'interaction avec l'interface. On a vu que le *desktop film* *Noah* se
sert d'interfaces pour multiplier les niveaux narratifs. Il repose
également sur l'aisance interactive dont fait preuve le personnage
principal. L'interaction est ainsi productrice du récit, à la manière
d'un acte narratif. Un rapport entre l'interaction et la narration
s'installe alors. Serge Bouchardon exprime bien la tension entre
narration et interaction : « La *narrativité* consiste en effet à
prendre le lecteur *par la main* pour lui raconter une histoire, du
début à la fin. L'*interactivité*, quant à elle, consiste *à donner la
main* au lecteur pour intervenir au cours du récit[^13]. » Toutefois,
une interaction reste un élément d'un programme, c'est-à-dire que ce est
permis et ce qui en découle a été conditionné au préalable. L'engagement
par l'interaction n'est compris que dans ce qui est autorisé et a pour
conséquence que l'interface se porte garante que les actions auront bien
l'effet escompté. On pourrait alors dire que l'interactivité est moins
une liberté qu'une confiance accordée.

L'œuvre *Portrait n^o^1* (Luc Courchesne, 1990) présente un certain
nombre de questionnements quant à l'interaction avec une interface.
Œuvre d'art mediale, sa conservation et sa diffusion ont occasionné sa
restauration dans différentes techniques et supports : outre sa version
originale (1990), il existe trois adaptations (CD-ROM en 1995, Flash en
2001 et HTML5 en 2019)[^14]. Cette œuvre consiste en un parcours
interactif dans des séquences vidéos orchestrées par des répliques
pré-écrites. Le dispositif original mettait en scène une certaine
qualité de présence que Gilles Rouffineau décrit en ces mots : « Dans
une ambiance sombre, le reflet d'un visage féminin (Marie) derrière une
vitre inclinée produit l'illusion d'une présence diaphane obtenue avec
un miroir sans tain[^15] ». Bien que ce dispositif disparaisse dans les
adaptations ultérieures, le principe de navigation reste identique :
faire des choix dans des répliques auxquelles Marie répond. *Portrait
n^o^1* offre un mélange d'interactions permises et d'interactions
empêchées. Le rapport à cette œuvre est celui
d'une conversation captive, d'une tentative de rencontre dont la
distance infranchissable est sans cesse rappelée dans un double
mouvement de l'interface au spectateur et du spectateur à l'interface.

*Portrait n^o^1* nous invite à imaginer Marie comme une interface qui
parle. Marie compare cette situation avec celle du roman *L'Invention de
Morel* (Adolfo Bioy Casares, 1940) où le narrateur tente de parler avec
une femme qui n'apparaît que par la projection d'un
enregistrement. Dans ces tentatives, il passe par trois phases : celle
où il croit qu'un échange est possible (« je pensai : ‹ elle se prépare
à m'adresser la parole ›. Cela ne se produisit pas[^16]. »), celle où il
réalise qu'il a tenté de parler à une enregistrement (« combien de fois
\[...\] j'ai parlé à son image[^17] »), et celle où il parvient à
s'intégrer à l'enregistrement (« j'intercale habilement quelque phrase
dans les siennes ; on dirait que Faustine me répond[^18] »). D'une perception illusoire
de l'originalité d'un enregistrement, le narrateur aboutit à l'utilisation
consciente de l'appareil afin de simuler une interface d'échange. Cette
expérience m'évoque la lecture que Pierre-Damien Huyghe propose de la
pensée de Walter Benjamin. Pour désigner l'époque de la photographie
Pierre-Damien Huyghe parle de l'époque de « l'expérience diffuse des
choses ». En cela, il décrit notre être au monde en coupure avec une
continuité spatio-temporelle originale au profit de sa diffusion par le
biais d'appareils et d'enregistrements. Il écrit notamment : « rien de
ce qui fait événement n'est véritablement localisé ». Le narrateur de
*L'Invention de Morel* rapporte une expérience similaire, il écrit
d'ailleurs : « il n'est pas impossible que toute absence ne soit en
définitive que spatiale... » Il pense ainsi que l'invention de Morel
peut remédier à l'absence. Cependant, en écrivant « un spectateur peu
averti peut croire que je ne suis pas un intrus », il fait le constat
que cela n'est qu'illusion. Pour reprendre les termes de Walter
Benjamin, le narrateur a un rapport sacré à l'enregistrement : il le
conçoit comme « l'apparition unique d'un lointain, aussi proche
soit-il ». En prêtant des capacités surnaturelles à un traitement de
données, il prend un media pour un médium[^19].

Le spectateur de *Portrait n^o^1* se trouve dans une situation
comparable : son imagination est mobilisée par le fait de de
s'enregistrer, de se diffuser dans un enregistrement. Le parcours que
propose cette œuvre est ainsi une expérience diffuse, « une expérience
qui n'a jamais eu lieu dans une continuité spatio-temporelle ». La
proposition interactive de cette œuvre met au jour la continuité qu'une
interface peut installer entre un enregistrement et un spectateur.

Cependant, cette continuité est un artifice dont le spectateur a
conscience. À la manière de la manipulation des niveaux narratifs par la
métalepse, les interactions avec Marie l'invitent à se montrer alerte
quant à la nature de ce qu'il regarde. À propos de la réception d'une
métalepse, Gérard Genette écrit d'ailleurs qu'elle « ne peut guère
attendre une pleine et entière suspension d'incrédulité, mais seulement
une simulation ludique de crédulité[^20]. » Ainsi, en suivant la
narration de *Matrix* ou en interragissant avec *Portrait n^o^1*,
l'interface met en place des continuités dont les caractéristiques
artificielles sont bien visibles.

Retrouve-t-on cette conscience d'une continuité artificielle dans les
interfaces graphiques que l'on pratique quotidiennement ? Je vais
revenir sur le court-métrage *Noah* et aussi sur le cas particulier de
la réception d'un *spam*.

## Un cadre laissé ouvert

Le court-métrage *Noah* active ce que Serge Bouchardon appelle une
métalepse dispositive. Pour peu qu'on le regarde sur un ordinateur,
l'image épouse le cadre de l'écran comme une usurpation du dispositif
lectoriel du spectateur. Comme évoqué plus haut, l'interface est loin
d'être un simple décor. Les éléments graphiques servent d'une part aux
acteurs pour exprimer leurs émotions. D'autre part, ils servent à
fournir un cadre fonctionnel à cette fiction. On retrouve ici la
caractéristique de confiance accordée au moment d'une interaction. Le
trouble dans la réception de *Noah* vient du fait que les interfaces se
portent garantes du récit. Ce que je souhaite soutenir ici, c'est que
leur contribution au récit apparaît plus fiable, moins jouée que celle
des acteurs, comme si notre suspension de crédulité dépassait celle
envers le jeu des acteux. Cela est particulièrement visible lorsque Noah
usurpe l'identité numérique de son amie et publie alors des messages en
son nom, ou bien lorsqu'il se connecte sur Chatroulette pour rencontrer
des inconnus au hasard. Ces actions ouvrent la narration à une réalité
qui la dépasse : on imagine d'autres personnes connectées à Facebook ou
Chatroulette prenant inconsciemment part à une fiction sans disposer du
contexte. En extrapolant, on peut même imaginer que le spectateur
lui-même aurait pu intervenir dans le récit au moment de sa réalisation.
À ce moment, si l'on met de côté le travail de montage sur lequel repose
la narration, *Noah* était ouvert à l'arrivée d'autres actions dans son
récit et il me semble que le court-métrage cherche à réactiver
cette caractéristique lorsqu'il est visionné. La réalisation cherche à
nous faire croire que les actions se sont bien passées réellement sur
Facebook. *Noah* se sert d'interfaces pour troubler son caractère
fictionnel même.

La mise en fonctionnement des interfaces est conforme à ce que l'on
connaît d'elles si bien que l'adhésion au récit n'invite pas
nécessairement à être remise en cause. Une des particularités narratives
marquantes de *Noah* est l'usage des interfaces pour façonner un récit
dont les bornes sont ouvertes. Le trouble dans la réception de la
fiction pour cette œuvre vient de la multiplication des possibilités de
franchissement de niveaux narratifs.

Cette porosité entre la réalité et la fiction est présente dans le travail de
Joana Hadjithomas et Khalil Joreige. Laura U. Marks (professeure à
l'université Simon Fraser) reprend leurs termes pour
décrire leur méthode de travail : \[ils\] ‹ plantent le
cadre › et le laissent ouvert pour que la réalité externe y laisse une
empreinte, pour que ‹ les événements s'insèrent dans le cadre › ».
Un de leurs travaux entretient un rapport avec la question des interfaces graphiques. Les artistes ont en effet réalisé depuis 1999, une collection de *spams*, et plus particulièrement des *scams*, arrivant à
une somme importante : près de 4 000 scams. Cette collecte prend forme
dans le projet « Je dois tout d'abord m'excuser » qui a été présenté en
2014 dans l'exposition du même nom à la Villa Arson. 

Interrogés sur la fabrication des *scams*, les arnaqueurs rapprochent leur travail de la préparation d'un film :
ils choisissent des personnages, un contexte, des accessoires et
construisent une histoire. Ils ajoutent que l'histoire se doit d'entretenir un rapport trouble entre la réalité et la fiction, et surtou que son intrigue soit infinie pour que toute nouvelle avancée soit une étape de plus vers la persuasion. Les scams sont donc des récits fictionnels qui s'inspirent de
faits réels et dont le but est d'engager le récepteur dans leur récit.
On peut dire que ce procédé relève de la métalepse : l'arnaqueur cherche
à faire rentrer le monde du lecteur dans le monde de son récit. Cette
porosité entre la réalité et la fiction est présente à de nombreuses
reprises dans les oeuvres de Joana Hadjithomas et Khalil Joreige. Ce
*cadre* laissé ouvert pourrait évoquer l'interface et pourtant les
artistes ne mettent pas particulièrement en avant le rôle que
l'interface peut avoir dans la réception d'un scam. Or comme pour
*Noah*, je soutiens ici que l'interface tient une importance primordiale
pour donner une crédibilité à un récit. La réception d'un scam dans une
interface quotidienne, lui fournit un décor dont la banalité peut
conduire une personne à ne pas se poser de questions et à s'engager dans
la fiction.


[^1]: Giroud, G. (2021). L'interface de la représentation,
    représentations de l'interface. *Interfaces numériques*, 10 (1).
    https://doi.org/10.25965/interfaces-numeriques.4542

[^2]: Le design de la transparence, Une rhétorique au cœur des
    interfaces numériques; Loup Cellard et Anthony Masure, Multitudes,
    2018.

[^3]: Gérard Genette, *Figures III*, Paris : Seuil, 1972, p. 248.

[^4]: Gérard Genette, *Figures III*, Paris : Seuil, 1972, p. 245.

[^5]: Gérard Genette, *Figures III*, Paris : Seuil, 1972, p. 245.

[^6]: Bouchardon S. (2006). « Les récits littéraires interactifs »,
    Formules, n°10, juin 2006, NOESIS - Agnès Vienot éditeur, Paris,
    pp. 79-93.

[^7]: Bouchardon S. (2006). « Les récits littéraires interactifs »,
    Formules, n°10, juin 2006, NOESIS - Agnès Vienot éditeur, Paris,
    pp. 79-93.

[^8]: Anaïs Guilet, « Where's That Damn 'Espace' Key ?: L'écran relié au
    prisme des desktop movies », colloque *Monitorer le présent. L'écran
    à l'ère du soupçon*, le 7 et 8 novembre 2019, Université du Québec à
    Montréal.

[^9]: Gérard Genette, *Métalepse*, p. 14.

[^10]: Gérard Genette, *Métalepse*, p. 23.

[^11]: Gérard Genette, *Métalepse*, p. 26.

[^12]: Bouchardon S. (2006). « Les récits littéraires interactifs »,
    Formules, n°10, juin 2006, NOESIS - Agnès Vienot éditeur, Paris,
    pp. 79-93.

[^13]: Bouchardon S. (2006). « Les récits littéraires interactifs »,
    Formules, n°10, juin 2006, NOESIS - Agnès Vienot éditeur, Paris,
    pp. 79-93.

[^14]: L'adaptation en HTML5 est consultable à cette adresse :
    https://www.fondation-langlois.org/Portrait-no-1/. Des détails
    techniques sont disponibles sur la page des crédits :
    https://www.fondation-langlois.org/Portrait-no-1/credits.html ou
    bien sur le site de l'auteur : http://www.courchel.net/.

[^15]: Gilles Rouffineau, *Éditions off-line. Projet critique de
    publications numériques 1989-2001*, Paris : Éditions B42, 2018,
    p. 41.

[^16]: Adolfo Bioy Casares, *L'Invention de Morel*, traduit par Armand
    Pierhal, Paris, Éditions 10-18, 1992 (1940), p. 34.

[^17]: Adolfo Bioy Casares, *L'Invention de Morel*, traduit par Armand
    Pierhal, Paris, Éditions 10-18, 1992 (1940), p. 100.

[^18]: Adolfo Bioy Casares, *L'Invention de Morel*, traduit par Armand
    Pierhal, Paris, Éditions 10-18, 1992 (1940), p. 119.

[^19]: Je reprends ici la graphie que proposent Yves Citton et Estelle
    Doudet dans leur introduction à l'ouvrage collectif *Écologies de
    l'attention et archéologie des media*. Par *media*, ils désignent
    « tout ce qui peut être utilisé par les humains pour *enregistrer*
    dans le temps, *transmettre* dans l'espace et \[...\] *traiter*
    \[...\] des données ». Par *médium*, ils désignent « ce que \[les\]
    dispositifs et \[les\] imaginaires de communication ont pu, dans le
    passé comme aujourd'hui, susciter d'angoisse ou d'émerveillement
    paraissant relever d'une ‹ surnature › »

[^20]: Gérard Genette, *Métalepse*, p. 25.
